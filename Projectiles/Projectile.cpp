#include "Projectile.h"

/**
 * @brief Конструктор класса Projectile
 * @param launch Позиция запуска снаряда
 * @param target Позиция прицела
 */
Projectile::Projectile(const sf::Vector2f &launch, const sf::Vector2f &target) :m_targetPosition(target), m_launchPosition(launch){
    createShapeObject(launch);
}

/**
 * @brief Обновляет позицию снаряда
*/
void Projectile::updatePosition() {
    m_projectileShape.setPosition(m_projectileShape.getPosition()+m_velocity*m_movementCLock.getElapsedTime().asSeconds());
    m_movementCLock.restart();
}

/**
* @brief Проверяет, находится ли снаряд в пределах экрана
* @return true, если снаряд находится в пределах экрана; false в противном случае
*/
bool Projectile::isWithinScreenBounds() const {
    const bool withCondition= getPosition().x<0.0f || getPosition().x>Configuration::MainWindow::WIDTH;
    const bool heightCondition = getPosition().y<0.0f || getPosition().y > Configuration::MainWindow::HEIGHT;
    const bool left= (getPosition().x >= 364 and getPosition().y >= 364 and getPosition().y <= 748 and getPosition().x<= 520) ;
    const bool right= (getPosition().y>= 500 and getPosition().y <= 640 and getPosition().x >=890 and getPosition().x <=1024);
    const bool middle= (getPosition().x >= 1400  and getPosition().y >= 364 and getPosition().y <= 780  and getPosition().x<= 1545) ;

    if (withCondition || heightCondition || middle || right || left) { return false; }
    return true;
}

/**
 * @brief Возвращает текущую позицию снаряда
 * @return Текущая позиция снаряда
 */
sf::Vector2f Projectile::getPosition() const { return m_projectileShape.getPosition(); }

/**
 * @brief Устанавливает скорость снаряда
 * @param speed Скорость снаряда
 */
void Projectile::setSpeed(const float &speed) {
    if (speed <0.0f) { throw std::invalid_argument("invalid projectile speed given"); }
    const float dx=m_targetPosition.x-m_launchPosition.x;
    const float dy= m_targetPosition.y-m_launchPosition.y;
    const float length=std::sqrt(dx*dx+dy*dy);
    m_velocity=sf::Vector2f(dx/length*speed, dy/length*speed);
}

/**
 * @brief Создает форму снаряда
 * @param shapePosition Позиция формы снаряда
 */
void Projectile::createShapeObject(const sf::Vector2f &shapePosition) {
   m_projectileShape.setSize(sf::Vector2f(10.0f, 10.0f));
   m_projectileShape.setFillColor(sf::Color::Red);
   m_projectileShape.setPosition(shapePosition.x,shapePosition.y);
}

/**
 * @brief Возвращает форму снаряда
 * @return Форма снаряда
 */
sf::RectangleShape Projectile::getRectangleShape() { return this->m_projectileShape; }

Shell::Shell(const sf::Vector2f &launch, const sf::Vector2f &target)  : Projectile(launch, target){
    setSpeed(kDefaultSpeed);
}

