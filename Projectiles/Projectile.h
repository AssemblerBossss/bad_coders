#ifndef TANK_PRO_PROJECTILE_H
#define TANK_PRO_PROJECTILE_H

#include <cmath>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "../Entity/entity.h"
#include "../Tank/Turret.hpp"
#include "Config.h"

/**
 * @brief Класс, представляющий снаряд
 */
class Projectile : public Entity {
private:
    sf::RectangleShape m_projectileShape;         ///< Сам снаряд, представляющий из себя sf::RectangleShape
    sf::Clock m_movementCLock;                    ///< Скорость перезарядки
    sf::Vector2f m_velocity;                      ///< Скорость снаряда sf::Vector2f
    sf::Vector2f m_targetPosition;                ///< позиция прицела
    sf::Vector2f m_launchPosition;                ///< место запуска

protected:
/**
 * @brief Обновляет позицию снаряда
*/
    void updatePosition();

/**
 * @brief Конструктор класса Projectile
 * @param launch Позиция запуска снаряда
 * @param target Позиция прицела
 */
    explicit Projectile(const sf::Vector2f &launch, const sf::Vector2f &target);

/**
 * @brief Устанавливает скорость снаряда
 * @param speed Скорость снаряда
 */
    void setSpeed(const float &speed);

/**
 * @brief Создает форму снаряда
 * @param shapePosition Позиция формы снаряда
 */
    void createShapeObject(const sf::Vector2f &shapePosition);

public:
/**
 * @brief Проверяет, находится ли снаряд в пределах экрана
 * @return true, если снаряд находится в пределах экрана; false в противном случае
*/
    [[nodiscard]] bool isWithinScreenBounds() const; // находится ли в пределах экрана

/**
 * @brief Возвращает текущую позицию снаряда
 * @return Текущая позиция снаряда
 */
    [[nodiscard]] sf::Vector2f getPosition() const;

/**
 * @brief Отрисовывает снаряд на окне
 * @param window Окно для отрисовки
 */
    void drawOn(sf::RenderWindow &window) override {
        window.draw(m_projectileShape);
        updatePosition();
    }

/**
 * @brief Возвращает форму снаряда
 * @return Форма снаряда
 */
    sf::RectangleShape getRectangleShape();

/**
 * @brief Деструктор класса Projectile
 */
    ~Projectile() override = default;
};

/**
 * @brief Класс, представляющий снаряд типа Shell
*/
class Shell final : public Projectile {
public:
    constexpr static float kDefaultSpeed = 500.0f;                   ///< Скорость снаряда по умолчанию

/**
 * @brief Конструктор класса Shell
 * @param launch Позиция запуска снаряда
 * @param target Позиция прицела
 */
    Shell(const sf::Vector2f &launch, const sf::Vector2f &target);

/**
 * @brief Конструктор класса Shell
 * @param turret Башня танка, с которой происходит запуск снаряда
 */
    explicit Shell(const Turret &turret) : Shell(turret.getMuzzlePosition(),
                                                 turret.getMuzzlePosition() + turret.getDirectionVector()) {
    }

/**
 * @brief Деструктор класса Shell
 */
    ~Shell() override = default;
};

#endif //TANK_PRO_PROJECTILE_H
