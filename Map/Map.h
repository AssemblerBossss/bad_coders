#pragma once
#include <SFML/Graphics.hpp>
using namespace sf;

/**
 * @class GameObject
 * @brief Класс игрового поля
 */
class Map {
public:
    static const int HEIGHT_MAP = 9; // map height
    const int WIDTH_MAP = 24;// map width

    /*std::string typeOfMap;*/

    sf::String summerMap[HEIGHT_MAP] = {
            "111111111111111",
            "1    2        1",
            "1          2  1",
            "1 23   2   3  1",
            "1  3  232  3  1",
            "1  3   2   3  1",
            "1 2         2 1",
            "12    2      21",
            "111111111111111",
    };

    sf::String autumnMap[HEIGHT_MAP] = {
            "111111111111111",
            "1    2   1    1",
            "1 1        2  1",
            "1 23   2   3  1",
            "1  3  232  3  1",
            "1  3   2   3  1",
            "1 2  1      2 1",
            "12    2   1  21",
            "111111111111111",
    };

    sf::String winterMap[HEIGHT_MAP] = {
            "111111111111111",
            "1    2   1    1",
            "1 1        2  1",
            "1 23   2   3  1",
            "1  3  232  3  1",
            "1  3   2   3  1",
            "1 2  1      2 1",
            "12    2   1  21",
            "111111111111111",
    };

    void drawMap(sf::RenderWindow &window, const std::string &typeOfMap);

    Map();

    sf::Texture texture;

    // Для летней карты
    sf::IntRect summer_bush;
    sf::IntRect grass;
    sf::IntRect summer_tower;
    sf::IntRect summer_dirt;

    sf::Sprite summer_bush_sprite;
    sf::Sprite grass_sprite;
    sf::Sprite summer_tower_sprite;
    sf::Sprite summer_dirt_sprite;

    // Для зимней карты
    sf::IntRect winter_bush;
    sf::IntRect snow;
    sf::IntRect winter_tower;
    sf::IntRect winter_dirt;

    sf::Sprite winter_bush_sprite;
    sf::Sprite snow_sprite;
    sf::Sprite winter_tower_sprite;
    sf::Sprite winter_dirt_sprite;

    // Для осенней карты
    sf::IntRect autumn_bush;
    sf::IntRect ground;
    sf::IntRect autumn_tower;
    sf::IntRect autumn_dirt;

    sf::Sprite autumn_bush_sprite;
    sf::Sprite ground_sprite;
    sf::Sprite autumn_tower_sprite;
    sf::Sprite autumn_dirt_sprite;
};