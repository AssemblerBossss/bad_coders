
#include "Map.h"

Map::Map() {
    this->texture.loadFromFile("../Image/Merged_document.png");

    // Для летней карты
    this->summer_bush = sf::IntRect (256, 128, 128, 128);
    this->grass = sf::IntRect (0, 128, 128, 128);
    this->summer_tower = sf::IntRect (0, 896, 128, 128);
    this->summer_dirt = sf::IntRect (128, 128, 128, 128);

    this->summer_bush_sprite = sf::Sprite(texture, summer_bush);
    this->grass_sprite = sf::Sprite(texture, grass);
    this->summer_tower_sprite = sf::Sprite(texture, summer_tower);
    this->summer_dirt_sprite = sf::Sprite(texture, summer_dirt);

    // Для зимней карты
    this->winter_bush = sf::IntRect (256, 0, 128, 128);
    this->snow = sf::IntRect (0, 0, 128, 128);
    this->winter_tower = sf::IntRect (0, 1536, 128, 128);
    this->winter_dirt = sf::IntRect (128, 0, 128, 128);

    this->winter_bush_sprite = sf::Sprite(texture, winter_bush);
    this->snow_sprite = sf::Sprite(texture, snow);
    this->winter_tower_sprite = sf::Sprite(texture, winter_tower);
    this->winter_dirt_sprite = sf::Sprite(texture, winter_dirt);

    // Для осенней карты
    this->autumn_bush = sf::IntRect (256, 256, 128, 128);
    this->ground = sf::IntRect (0, 256, 128, 128);
    this->autumn_tower = sf::IntRect (0, 2176, 128, 128);
    this->autumn_dirt = sf::IntRect (128, 256, 128, 128);

    this->autumn_bush_sprite = sf::Sprite(texture, autumn_bush);
    this->ground_sprite = sf::Sprite(texture, ground);
    this->autumn_tower_sprite = sf::Sprite(texture, autumn_tower);
    this->autumn_dirt_sprite = sf::Sprite(texture, autumn_dirt);
}

void Map::drawMap(sf::RenderWindow& window, const std::string &mapType) {

    if (mapType == "summer")
    {
        for (int i = 0; i < HEIGHT_MAP; i++) {
            for (int j = 0; j < WIDTH_MAP; j++) {
                if (summerMap[i][j] == '1') {
                    summer_bush_sprite.setPosition(j * 128, i * 128);
                    window.draw(summer_bush_sprite);
                }
                if (summerMap[i][j] == ' ') {
                    grass_sprite.setPosition(j * 128, i * 128);
                    window.draw(grass_sprite);
                }
                if (summerMap[i][j] == '2') {
                    summer_dirt_sprite.setPosition(j * 128, i * 128);
                    window.draw(summer_dirt_sprite);
                }
                if (summerMap[i][j] == '3') {
                    summer_tower_sprite.setPosition(j * 128, i * 128);
                    window.draw(summer_tower_sprite);
                }
            }
        }
    }

    if (mapType == "winter")
    {
        for (int i = 0; i < HEIGHT_MAP; i++) {
            for (int j = 0; j < WIDTH_MAP; j++) {
                if (winterMap[i][j] == '1') {
                    winter_bush_sprite.setPosition(j * 128, i * 128);
                    window.draw(winter_bush_sprite);
                }
                if (winterMap[i][j] == ' ') {
                    snow_sprite.setPosition(j * 128, i * 128);
                    window.draw(snow_sprite);
                }
                if (winterMap[i][j] == '2') {
                    winter_dirt_sprite.setPosition(j * 128, i * 128);
                    window.draw(winter_dirt_sprite);
                }
                if (winterMap[i][j] == '3') {
                    winter_tower_sprite.setPosition(j * 128, i * 128);
                    window.draw(winter_tower_sprite);
                }
            }
        }
    }

    if (mapType == "autumn")
    {
        for (int i = 0; i < HEIGHT_MAP; i++) {
            for (int j = 0; j < WIDTH_MAP; j++) {
                if (autumnMap[i][j] == '1') {
                    autumn_bush_sprite.setPosition(j * 128, i * 128);
                    window.draw(autumn_bush_sprite);
                }
                if (autumnMap[i][j] == ' ') {
                    ground_sprite.setPosition(j * 128, i * 128);
                    window.draw(ground_sprite);
                }
                if (autumnMap[i][j] == '0') {
                    autumn_tower_sprite.setPosition(j * 128, i * 128);
                    window.draw(autumn_tower_sprite);
                }
                if (autumnMap[i][j] == '2') {
                    autumn_dirt_sprite.setPosition(j * 128, i * 128);
                    window.draw(autumn_dirt_sprite);
                }
                if (autumnMap[i][j] == '3') {
                    autumn_tower_sprite.setPosition(j * 128, i * 128);
                    window.draw(autumn_tower_sprite);
                }
            }
        }
    }
}