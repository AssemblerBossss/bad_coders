//
// Created by max on 14.12.2023.
//

#ifndef TANK_PRO_MAPCHOICE_H
#define TANK_PRO_MAPCHOICE_H
#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>
using namespace std;
struct MapChoice
{
    sf::Sprite _mapSprite;
    sf::Text _mapName;
    sf::Texture _mapTexture;
    sf::Font _map_font;

    MapChoice() {

    }

    MapChoice(const std::string& name, const std::string& path)
    {
        _map_font.loadFromFile("../Image/Shrift.ttf");

        _mapName.setFont(_map_font);
        _mapName.setFillColor(sf::Color::White);
        _mapName.setString(name);
        _mapName.setCharacterSize(60);
        _mapName.setOutlineThickness(3);
        _mapName.setOutlineColor(sf::Color::Black);

        _mapTexture.loadFromFile(path);

        _mapSprite.setTexture(_mapTexture);
    }

    int setMap(sf::RenderWindow& window)
    {
        MapChoice winter("winter", "../Image/ground_shaker_asset/Terrains/winter.png");
        MapChoice summer("summer", "../Image/ground_shaker_asset/Terrains/leto.png");
        MapChoice autumn("autumn", "../Image/ground_shaker_asset/Terrains/leto3.png");

        std::vector<MapChoice> maps = {winter, summer, autumn};

        sf::RectangleShape mapChoice;
        mapChoice.setSize(sf::Vector2f(1920, 1080));
        sf::Texture texture_map;
        texture_map.loadFromFile("../Image/mapChoice.jpg");
        mapChoice.setTexture(&texture_map);

        sf::RectangleShape mapChoice1;
        mapChoice1.setSize(sf::Vector2f(1920, 1080));
        sf::Texture texture_map1;
        texture_map1.loadFromFile("../Image/menu2.jpg");
        mapChoice1.setTexture(&texture_map1);

        float spacing = 400.0f;
        float spacing2 = 437.0f;
        float startTankX = 390.0f;
        float startTextX = 450.0f;

        for (auto & map : maps) {
            map._mapSprite.setPosition(startTankX, 300.0f);
            map._mapSprite.setOrigin(map._mapSprite.getGlobalBounds().width / 2, map._mapSprite.getGlobalBounds().height / 2);
            map._mapName.setPosition(startTankX, 450.0f);
            map._mapName.setOrigin(map._mapName.getGlobalBounds().width / 2, map._mapName.getGlobalBounds().height / 2);
            startTankX += map._mapSprite.getGlobalBounds().width + spacing;
            startTextX += map._mapName.getGlobalBounds().width + spacing2;
        }

        int static  selectedMap = 0;
        int static totalMap = 0;

        bool returnPressed = false;

        sf::Event event{};
        while (event.key.code != sf::Keyboard::S) {
            while (window.pollEvent(event)) {
                switch (event.type) {
                    case sf::Event::Closed:
                        window.close();
                        break;
                    case sf::Event::KeyPressed:
                        switch (event.key.code) {
                            case sf::Keyboard::Left:
                                selectedMap = (selectedMap - 1 + 3) % 3;
                                break;
                            case sf::Keyboard::Right:
                                selectedMap = (selectedMap + 1) % 3;
                                break;
                            case sf::Keyboard::S:
                                if (selectedMap >= 0 && selectedMap < 3) {
                                    totalMap = selectedMap + 1;
                                    returnPressed = true;
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }

            }

            window.draw(mapChoice);

            for (size_t i = 0; i < maps.size(); ++i) {
                window.draw(maps[i]._mapSprite);
                window.draw(maps[i]._mapName);

                if (i == selectedMap) {
                    maps[i]._mapName.setFillColor(sf::Color::Yellow);
                    maps[i]._mapSprite.setScale(1.5f, 1.5f);
                }
                else {
                    maps[i]._mapName.setFillColor(sf::Color::White);
                    maps[i]._mapSprite.setScale(1.0f, 1.0f);
                }
            }
            window.display();

            if (returnPressed) {
                return totalMap;
            }
        }

        return 0;
    }
};
#endif //TANK_PRO_MAPCHOICE_H
