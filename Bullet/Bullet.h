#ifndef TANK_PRO_BULLET_H
#define TANK_PRO_BULLET_H

#include <SFML/Graphics.hpp>
#include <list>

/**
 * @class GameObject
 * @brief Базовый класс для игровых объектов
 */
class GameObject {
public:
    sf::Texture texture; /**< Текстура объекта */
    sf::Sprite sprite; /**< Спрайт объекта */

    /**
     * @brief Виртуальная функция для отрисовки объекта
     * @param window Окно, в котором будет отрисовываться объект
     */
    virtual void Draw(sf::RenderWindow& window) = 0;
};

/**
 * @class Bullet
 * @brief Класс для пуль
 * @inherit GameObject
 */
class Bullet : public GameObject {
public:
    /**
     * @brief Перечисление направлений движения пули
     */
    enum  Direction {Up,Down,Left, Right};
    Direction dir; /**< Направление пули */
    sf::Vector2f vectorOfMovement; /**< Вектор движения пули */

    /**
        * @brief Конструктор пули
        * @param direction Направление пули
     */
    explicit Bullet(sf::Vector2f direction);

    /**
        * @brief Виртуальный деструктор пули
     */
    virtual ~Bullet() {}

    float BulletDefaultMovementSpeed;                                    /**< Скорость движения пули по умолчанию */
    bool isFrozen = false;                                               /**< Флаг замороженности пули */

    /**
        * @brief Конструктор пули
        * @param dir Направление пули
     */
    explicit Bullet(Direction dir);

    /**
        * @brief Виртуальная функция для движения пули
     */
    virtual void Move() = 0;

    /**
        * @brief Функция для отрисовки пули
        * @param window Окно, в котором будет отрисовываться пуля
     */
    void Draw(sf::RenderWindow& window) override;

    /**
        * @brief Функция для остановки движения пули
     */
    void Stop();

    /**
        * @brief Функция для запуска движения пули
     */
    void Start();

    /**
        * @brief Функция для получения спрайта пули
        * @return Спрайт пули
     */
    sf::Sprite getSprite() const{
        return sprite;
    }
};

/**
    * @class EnemyBullet
    * @brief Класс для пуль врагов
    * @inherit Bullet
 */
class EnemyBullet : public Bullet {
public:
    /**
        * @brief Конструктор пули врага
        * @param dir Направление пули
     */
    explicit EnemyBullet(Direction dir);

    /**
        * @brief Конструктор пули врага
        * @param direction Направление пули
     */
    explicit EnemyBullet(sf::Vector2f direction);

    /**
        * @brief Функция для движения пули врага
     */
    void Move() override;

    float BulletMovementSpeed = BulletDefaultMovementSpeed;                  /**< Скорость движения пули врага */
};

static std::list<Bullet*> bulletList;                                        /**< Список пуль */
#endif //TANK_PRO_BULLET_H
