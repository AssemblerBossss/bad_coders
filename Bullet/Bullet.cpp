#include "Bullet.h"
#include "cmath"

/**
    * @brief Конструктор класса Bullet.
    * @param dir Направление снаряда.
 */
Bullet::Bullet(Direction dir) {
    this->texture.loadFromFile("../Image/bullet.png");
    this->sprite.setTexture(texture);
    this->sprite.setScale(1.0f, 1.0f);
    this->BulletDefaultMovementSpeed = 3.0f;
    this->sprite.setOrigin(sf::Vector2f(this->texture.getSize() / 2u));
    this->dir = dir;
    switch (dir) {
        case Direction::Up:
            this->sprite.setRotation(0);
            break;
        case Direction::Down:
            this->sprite.setRotation(180);
            break;
        case Direction::Left:
            this->sprite.setRotation(270);
            break;
        case Direction::Right:
            this->sprite.setRotation(90);
            break;
    }
}

/**
    * @brief Новый конструктор класса Bullet, который устанавливает направление снаряда в зависимости от расстояния до базы.
    * @param direction Направление снаряда.
 */
Bullet::Bullet(sf::Vector2f direction) : vectorOfMovement(direction) {
    this->texture.loadFromFile("../Image/bullet.png");
    this->sprite.setTexture(texture);
    this->sprite.setScale(1.0f, 1.0f);
    this->BulletDefaultMovementSpeed = 3.0f;

    // Устанавливаем угол полета снаряда
    float angle = std::atan2(direction.y, direction.x) * 180 / M_PI;
    this->sprite.setRotation(angle);
}

/**
    * @brief Конструктор класса EnemyBullet.
    * @param dir Направление снаряда.
 */
EnemyBullet::EnemyBullet(Direction dir) : Bullet(dir) {}

/**
    * @brief Новый конструктор класса EnemyBullet, который устанавливает направление снаряда в зависимости от расстояния до базы.
    * @param direction Направление снаряда.
 */
EnemyBullet::EnemyBullet(sf::Vector2f direction) : Bullet(direction) {}

/**
    * @brief Отрисовка снаряда.
    * @param window Окно, в котором происходит отрисовка.
 */
void Bullet::Draw(sf::RenderWindow& window) {
    window.draw(this->sprite);
}

/**
    * @brief Перемещение снаряда.
 */
void EnemyBullet::Move() {
    if (!isFrozen) {
        BulletMovementSpeed = BulletDefaultMovementSpeed;
        switch (dir) {
            case Direction::Up:
                this->sprite.move(0, -this->BulletMovementSpeed);
                break;
            case Direction::Down:
                this->sprite.move(0, this->BulletMovementSpeed);
                break;
            case Direction::Left:
                this->sprite.move(-this->BulletMovementSpeed, 0);
                break;
            case Direction::Right:
                this->sprite.move(this->BulletMovementSpeed, 0);
                break;
        }

        // Здесь запускаем пули в направлении к базе
        this->sprite.move(vectorOfMovement * this->BulletMovementSpeed);
    } else {
        BulletMovementSpeed = 0;
    }
}

/**
    * @brief Остановка снаряда.
 */
void Bullet::Stop() {
    isFrozen = true;
}

/**
    * @brief Запуск снаряда.
 */
void Bullet::Start() {
    isFrozen = false;
}

