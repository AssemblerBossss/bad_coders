//
// Created by max on 20.12.2023.
//

#ifndef TANK_PRO_HEALTHPACK_H
#define TANK_PRO_HEALTHPACK_H
#include <SFML/Graphics.hpp>

class HealthPack
{
public:
    HealthPack() : position(0.f, 0.f), lastSpawnTime(-5.f) {
        srand(static_cast<unsigned int>(time(nullptr)));
        if (!texture.loadFromFile("../Image/aid.png")) {
        }
        healthPackSprite.setTexture(texture);
        healthPackSprite.setScale(0.35f, 0.35f);
        healthPackSprite.setPosition(1300.f, 200.f);
    }

    sf::Vector2f getPosition() const {
        return position;
    }

    sf::Sprite getSprite() {
        return healthPackSprite;
    }

    void drawPack(sf::RenderWindow& window) {
        sf::Vector2u windowSize = window.getSize();

        // Проверяем, прошло ли 5 секунд с момента последнего респауна
        float elapsedTime = clock.getElapsedTime().asSeconds();
        if (elapsedTime - lastSpawnTime > 15.f) {
            // Генерация новой позиции аптечки
            position.x = static_cast<float>(rand() % windowSize.x);  // Рандомная координата x
            position.y = static_cast<float>(rand() % windowSize.y);  // Рандомная координата y
            healthPackSprite.setPosition(position);

            // Обновляем время последнего респауна
            lastSpawnTime = elapsedTime;
        }

        window.draw(healthPackSprite);
    }

private:
    sf::Vector2f position;
    sf::Sprite healthPackSprite;
    sf::Texture texture;
    sf::Clock clock;
    float lastSpawnTime;
};










#endif //TANK_PRO_HEALTHPACK_H
