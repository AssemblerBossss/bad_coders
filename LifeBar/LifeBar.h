#include "SFML/Graphics.hpp"

#ifndef TANK_PRO_LIFEBAR_H
#define TANK_PRO_LIFEBAR_H

/**
 * @class GameObject
 * @brief Класс, описывающий полоску здоровья
 */
class LifeBar {
public:
    sf::Image LifeBarImage;                          /**< Изображение полоски здоровья */
    sf::Texture LifeBarTexture;                      /**< Текстура полоски здоровья */
    sf::Sprite LifeBarSprite;                        /**< Спрайт полоски здоровья */
    int max;                                         /**< Максимальное значение здоровья */
    sf::RectangleShape bar;                          /**< Черный прямоугольник, покрывающий полоску здоровья */

    /**
     *  @brief Конструктор класса LifeBar */
    LifeBar();

    /**
    * @brief Метод обновления полоски здоровья
    * @param k Текущее значение здоровья
    */
    void update(int k);

    /**
    * @brief Метод отрисовки полоски здоровья
    * @param window Окно, на котором будет отрисована полоска здоровья
    * @param xSprite Позиция X спрайта (полоска здоровья)
    * @param ySprite Позиция Y спрайта (полоска здоровья)
    * @param xBar Позиция X черного прямоугольника
    * @param yBar Позиция Y черного прямоугольника
    */
    void draw(sf::RenderWindow &window, float xSprite, float ySprite, float xBar, float yBar);
};

/**
  * @brief Конструктор класса LifeBar
  */
LifeBar::LifeBar() {
    LifeBarImage.loadFromFile("../Image/LifeBar.png");
    LifeBarImage.createMaskFromColor(sf::Color(50, 96, 166));
    LifeBarTexture.loadFromImage(LifeBarImage);
    LifeBarSprite.setTexture(LifeBarTexture);
    LifeBarSprite.setTextureRect(sf::IntRect(783, 2, 15, 84));
    LifeBarSprite.setScale(2.0f,2.0f);

    bar.setFillColor(sf::Color(0, 0, 0)); // черный прямоугольник накладывается сверху
    max = 100; // и появляется эффект отсутствия здоровья
}

/** Метод обновления полоски здоровья
* @param k Текущее значение здоровья
*/
void LifeBar::update(int k) {
    if (k >= 0 && k <= max) {
        bar.setSize(sf::Vector2f(16, (max - k) * 140 / max)); // если не отрицательно и при этом меньше максимума,
    }                                                        // то устанавливаем новое значение (новый размер) для черного прямоугольника
}

/** Метод отрисовки полоски здоровья
* @param window Окно, на котором будет отрисована полоска здоровья
* @param xSprite Позиция X спрайта (полоска здоровья)
* @param ySprite Позиция Y спрайта (полоска здоровья)
* @param xBar Позиция X черного прямоугольника
* @param yBar Позиция Y черного прямоугольника
*/
void LifeBar::draw(sf::RenderWindow &window, float xSprite, float ySprite, float xBar, float yBar) {
    sf::Vector2f center = window.getView().getCenter();
    sf::Vector2f size = window.getView().getSize();

    LifeBarSprite.setPosition(center.x - size.x / 2 + xSprite, center.y - size.y / 2 + ySprite); // позиция на экране
    bar.setPosition(center.x - size.x / 2 + xBar, center.y - size.y / 2 + yBar);

    window.draw(LifeBarSprite); // сначала рисуем полоску здоровья
    window.draw(bar); // поверх неё уже черный прямоугольник, он как бы покрывает её
}

#endif //TANK_PRO_LIFEBAR_H


