//
// Created by dmitryi on 03.12.2023.
//

#ifndef TANK_PRO_ENEMYBAR_H
#define TANK_PRO_ENEMYBAR_H
#include "SFML/Graphics.hpp"

/**
 * @class EnemyBar
 * @brief Класс, отвечающий за отображение полоски врагов на экране
 */
class EnemyBar
{
public:
    sf::Image EnemyBarImage;                         /**< Изображение полоски врагов */
    sf::Texture EnemyBarTexture;                     /**< Текстура полоски врагов */
    sf::Texture PlayerTexture;                       /**< Текстура игрока */
    std::vector<sf::Sprite> EnemyBarSprites;         /**< Вектор спрайтов врагов */
    sf::Sprite PlayerSprite;                         /**< Спрайт игрока */
    sf::Font font;                                   /**< Шрифт */
    sf::Text text;                                   /**< Текст "Enemies: " */

    /**
     * @brief Конструктор класса EnemyBar.
     * Загружает текстуры, устанавливает параметры текста и позиции спрайтов.
     */
    EnemyBar();

    /**
     * @brief Обновляет отображение полоски врагов.
     * @param EnemyCount - количество врагов
     */
    void update(size_t EnemyCount);

    /**
     * @brief Отрисовывает полоску врагов на заданном окне.
     * @param window - окно для отрисовки
     */
    void drawOn(sf::RenderWindow & window);

};


/**
 * @brief Конструктор класса EnemyBar.
 * Загружает текстуры, устанавливает параметры текста и позиции спрайтов.
 */
EnemyBar::EnemyBar() {
    EnemyBarTexture.loadFromFile("../Image/ground_shaker_asset/Red/Bodies/red.png");
    font.loadFromFile("../Image/Shrift.ttf");
    text.setCharacterSize(40);
    text.setFillColor(sf::Color::Black);
    text.setFont(font);
    text.setString("Enemies: ");
    text.setPosition(1080, 5);


    PlayerTexture.loadFromFile("../Image/camo.png");
    PlayerSprite.setTexture(PlayerTexture);
    PlayerSprite.setScale(0.5f, 0.5f);
    PlayerSprite.setPosition(50,5);

}

/**
 * @brief Отрисовывает полоску врагов на заданном окне.
 * @param window - окно для отрисовки
 */
void EnemyBar::drawOn(sf::RenderWindow &window) {
    window.draw(text);
    window.draw(PlayerSprite);
    for (const auto & EnemySprite: EnemyBarSprites){
        window.draw(EnemySprite);
    }
}

/**
 * @brief Обновляет отображение полоски врагов.
 * @param EnemyCount - количество врагов
 */
void EnemyBar::update(size_t EnemyCount) {
    EnemyBarSprites.clear();
    for (int i = 0; i < EnemyCount; i++) {
        sf::Sprite tankSprite;
        tankSprite.setTexture(EnemyBarTexture);
        tankSprite.setScale(0.5f,0.5f);
        tankSprite.setPosition(1100 +( EnemyBarTexture.getSize().x + i * (EnemyBarTexture.getSize().x)/2u ), 5);
        EnemyBarSprites.push_back(tankSprite);
    }
}


#endif //TANK_PRO_ENEMYBAR_H