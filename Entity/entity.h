//
// Created by mihail on 23.10.2023.
//
#pragma once
#include <SFML/Graphics.hpp>
#ifndef TANK_PRO_ENTITY_H
#define TANK_PRO_ENTITY_H

/** \file
 * \brief Заголовочный файл с описанием класса Entity
 */

/**
 * \class Entity
 * \brief Базовый абстрактный класс сущностей
 *
 * Класс определяет интерфейс для отрисовки сущностей в окне
 */
class Entity{
public:
    /**
     * \brief Виртуальный метод отрисовки сущности
     *
     * \param window Окно для отрисовки
     */
    virtual void drawOn(sf::RenderWindow & window) = 0;

    /**
     * \brief Виртуальный деструктор класса Entity
     *
     * Виртуальный деструктор позволяет правильно освободить память при удалении
     * объекта производного класса через указатель на базовый класс
     */
    virtual ~Entity() = default;
};
#endif //TANK_PRO_ENTITY_H
