#include "tankBot.h"
#include <random>

/**
    * @brief Отрисовывает спрайт бота на заданном окне.
    * @param window Окно, на котором отрисовывается спрайт.
 */
void tankBot::drawOn(sf::RenderWindow &window) {

    if (isLife) {
        this->moveRandom(window);
        this->BotBarrierBump(window);
        window.draw(BotSprite);
    }
}

tankBot::tankBot() {
    currentDirection = Up;
    BotSetTexture();
}

/**
    * @brief Устанавливает текстуру для спрайта бота.
 */
void tankBot::BotSetTexture() {                                                  // Загрузка текстуры танка-бота
    if (!BotTexture.loadFromFile("../Image/track" + std::to_string(NumberOfTheTankEquipment % 3)+".png")) {
        std::cerr << "Failed to load tank bot texture!" << std::endl;
    }
    NumberOfTheTankEquipment++;
    BotSprite.setTexture(BotTexture);
    BotBuildSprite();
}

/**
    * @brief Метод, отвечающий за построение бота.
 */
void tankBot::BotBuildSprite() {
     sf::Vector2f startPostion(300.f, 150.f);
    BotSprite.setScale(SpriteScale, SpriteScale);
    BotSprite.setPosition(startPostion);
    BotSprite.setOrigin(sf::Vector2f(BotTexture.getSize() / 2u));
    BotPosition = startPostion;
}

/**
    * @brief Возвращает текущую позицию бота на экране.
 */
sf::Vector2f tankBot::getPosition() const {
    return BotSprite.getPosition();
}

/**
    * @brief Двигает бота случайным образом на заданном окне.
    * @param window Окно, на котором двигается бот.
 */
void tankBot::moveRandom(sf::RenderWindow& window) {
    srand(time(NULL));

    int randomNumber = rand() % 4;
    if(!isFrozen) {
        BotMovementSpeed = BotDefaultMovementSpeed;
        if (clock.getElapsedTime().asSeconds() >= 3) {
            //currentDirection = static_cast<Direction>(rand() % 4);                                        // Генерируем случайное число от 0 до 3 для выбора нового направления
            currentDirection = static_cast<Direction>( randomNumber);                                       // Генерируем случайное число от 0 до 3 для выбора нового направления
            clock.restart();                                                                                // Сбрасываем таймер
        }
        int Angle = 0;
        if (BotSprite.getPosition().x < 10) {
            currentDirection = Right;
        } else if (BotSprite.getPosition().y < 10) {
            currentDirection = Down;
        } else if (BotSprite.getPosition().y > 1060) {
            currentDirection = Up;
        } else if (BotSprite.getPosition().x > 1900) {
            currentDirection = Left;
        }
        if (currentDirection == Up) {                                                                       // Обновляем позицию танка в зависимости от текущего направления
            BotPosition.y -= BotMovementSpeed;
            Angle = 270;
        } else if (currentDirection == Down) {
            BotPosition.y += BotMovementSpeed;
            Angle = 90;
        } else if (currentDirection == Left) {
            BotPosition.x -= BotMovementSpeed;
            Angle = 180;
        } else if (currentDirection == Right) {
            BotPosition.x += BotMovementSpeed;
            Angle = 0;
        }
        BotSprite.setRotation(Angle);
        BotSprite.setPosition(BotPosition);  // Обновляем позицию спрайта
    } else{
        BotMovementSpeed = 0;
    }

}

/**
    * @brief Обрабатывает столкновение бота с препятствием.
    * @param window Окно, в котором находится препятствие.
 */
void tankBot::BotBarrierBump(sf::RenderWindow & window) {
    if (BotSprite.getPosition().x >= 364 and BotSprite.getPosition().y >= 364 and BotSprite.getPosition().y <= 748 and BotSprite.getPosition().x <= 492) {  //левое препятствие при подъезде слева
        currentDirection = Up;
    }
    if (BotSprite.getPosition().x >= 512 and BotSprite.getPosition().x <= 520 and BotSprite.getPosition().y >= 364 and BotSprite.getPosition().y <= 748) { //левое препятствие при подъезде справа
        currentDirection = Right;
    }
    if (BotSprite.getPosition().x >= 364 and BotSprite.getPosition().y == 364 and BotSprite.getPosition().x <= 492) { //левое препятствие при подъезде сверху
        currentDirection = Up;
    }
    if (BotSprite.getPosition().x >= 364 and BotSprite.getPosition().x <= 492 and BotSprite.getPosition().y == 780) {//левое препятствие при подъезде снизу
        currentDirection = Right;
    }
    if (BotSprite.getPosition().x >= 364 and BotSprite.getPosition().x <= 492 and BotSprite.getPosition().y == 780) {//левое препятствие при подъезде снизу
            currentDirection = Right;
    }
    if (BotSprite.getPosition().y >= 500 and BotSprite.getPosition().y <= 640 and BotSprite.getPosition().x ==890) {//срединное препятствие при подъезде слева
        currentDirection = Left;
    }
    if (BotSprite.getPosition().y == 500 and BotSprite.getPosition().x>=890 and BotSprite.getPosition().x <=1024) {//срединное препятствие при подъезде сверху
        currentDirection = Up;
    }
    if (BotSprite.getPosition().y >= 500 and BotSprite.getPosition().y <= 640 and BotSprite.getPosition().x <=1035 and BotSprite.getPosition().x>=1010) {//срединное препятствие при подъезде справа
        currentDirection = Right;
    }
    if (BotSprite.getPosition().y == 644 and BotSprite.getPosition().x>=890 and BotSprite.getPosition().x <=1024) {//срединное препятствие при подъезде сверху
        currentDirection = Left;
    }
    if (BotSprite.getPosition().x >= 1400 and BotSprite.getPosition().y >= 364 and BotSprite.getPosition().y <= 748 and BotSprite.getPosition().x <= 1415) {  //правое  препятствие при подъезде слева
        currentDirection = Left;
    }
    if (BotSprite.getPosition().x >= 1530 and BotSprite.getPosition().x <= 1545 and BotSprite.getPosition().y >= 364 and BotSprite.getPosition().y <= 748) { //правое препятствие при подъезде справа
        currentDirection = Right;
    }
    if (BotSprite.getPosition().x >= 1400 and BotSprite.getPosition().y == 364 and BotSprite.getPosition().x <=1545) { //правое препятствие при подъезде сверху
        currentDirection = Left;
    }
    if (BotSprite.getPosition().x >= 1400 and BotSprite.getPosition().x <= 1545 and BotSprite.getPosition().y == 780) {//левое препятствие при подъезде снизу
        currentDirection = Left;
    }
}

/**
    * @brief Возвращает текущее направление движения бота.
 */
int tankBot::getCurrentDirection() const { return currentDirection; }

/**
    * @brief Останавливает движение бота.
 */
void tankBot::Stop() { isFrozen = true;}

/**
    * @brief Запускает движение бота.
 */
void tankBot::Start() { isFrozen = false; }

/**
    * @brief Возвращает значение флага, жив ли бот.
    * @return true - если бот жив, false - в противном случае.
 */
bool tankBot::getIsLife() const {return isLife;}

/**
    * @brief Обрабатывает столкновение бота с пулями игрока.
    * @param projectiles Список пуль игрока.
 */
void tankBot::checkCollisionWithPlayer(std::deque<std::unique_ptr<Projectile>>& projectiles) {
    sf::FloatRect spriteBounds = this->BotSprite.getGlobalBounds();
    for (const auto &projectile: projectiles) {
        auto rectBounds = projectile->getRectangleShape().getGlobalBounds();
        if (rectBounds.intersects(spriteBounds)) {
            /*setDeadBots();*/
            isLife = false;
            projectiles.front().reset();
            projectiles.pop_front();
        }
    }
}

/**
    * @brief Возвращает расстояние между двумя объектами типа Sprite.
    * @param sprite1 Первый спрайт.
    * @param sprite2 Второй спрайт.
    * @return float Расстояние между спрайтами.
 */
float tankBot::getDistance(const sf::Sprite &sprite1, const sf::Sprite &sprite2) {
    sf::Vector2f position1 = sprite1.getPosition();
    sf::Vector2f  position2 = sprite2.getPosition();
    float dx = position2.x - position1.x;
    float dy = position2.y - position1.y;
    return sqrt(dx * dx + dy * dy);
}

/**
    * @brief Метод, не позволяющий ботам наезжать друг на друга.
 */
void differentBotPosition(tankBot &firstBot, tankBot &secondBot) {
    auto firstPos=firstBot.BotSprite.getGlobalBounds();
    auto secondPos=secondBot.BotSprite.getGlobalBounds();
    if (firstPos.intersects(secondPos))
    {
        if (firstBot.getCurrentDirection() == tankBot::Direction::Left) {
            firstBot.currentDirection = tankBot::Direction::Right;
            secondBot.BotMovementSpeed = 0;
        }
        if (firstBot.getCurrentDirection() == tankBot::Direction::Down) {
            firstBot.currentDirection = tankBot::Direction::Right;
            secondBot.BotMovementSpeed = 0;
        }
        if (secondBot.getCurrentDirection() == tankBot::Direction::Right) {
            secondBot.currentDirection = tankBot::Direction::Left;
            firstBot.BotMovementSpeed = 0;
        }
        if (firstBot.getCurrentDirection() == tankBot::Direction::Down) {
            firstBot.currentDirection = tankBot::Direction::Left;
            secondBot.BotMovementSpeed = 0;
        }
       /* if (secondBot.getCurrentDirection() == tankBot::Direction::Left) {
            secondBot.currentDirection = tankBot::Direction::Up;
            firstBot.BotMovementSpeed = 0;
        }*/
      }
    secondBot.BotMovementSpeed=tankBot::BotDefaultMovementSpeed;
    firstBot.BotMovementSpeed=tankBot::BotDefaultMovementSpeed;


}








