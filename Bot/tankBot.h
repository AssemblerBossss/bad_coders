/**
    @file tankBot.h
    @brief Этот файл содержит объявление класса tankBot.
 */
#pragma once
#include "../Entity/entity.h"
#include "../Projectiles/Projectile.h"
#include "../Projectiles/Config.h"
#include "../Tank/MovementVectors.hpp"
#include "../Bullet/Bullet.h"
#include <iostream>
#include <memory>
#include <deque>
#include <ctime>
#include <list>
#include "../Projectiles/Projectile.h"
#include "../Projectiles/Config.h"
#include "../CommandPost/CommandPost.h"

#ifndef TANK_PRO_TANKBOT_H
#define TANK_PRO_TANKBOT_H

/**

    @brief Класс для представления бота-танка.
*/
static int  NumberOfTheTankEquipment = 1;
class tankBot : public Entity {
public:
    tankBot();
    ~tankBot() override=default;

    static constexpr float kMainWeaponSecondsToReload = 1.0f;
    constexpr static float BotDefaultMovementSpeed = 1.0f;
/**
    * @brief Устанавливает текстуру для спрайта бота.
 */
    void BotSetTexture();

/**
    * @brief Отрисовывает спрайт бота на заданном окне.
    * @param window Окно, на котором отрисовывается спрайт.
 */
    void drawOn(sf::RenderWindow &window) override;

/**
    * @brief Двигает бота случайным образом на заданном окне.
    * @param window Окно, на котором двигается бот.
 */
    void moveRandom(sf::RenderWindow& window);

/**
    * @brief Останавливает движение бота.
 */
    void Stop();

/**
    * @brief Запускает движение бота.
 */
    void Start();

/**
    * @brief Возвращает текущую позицию бота на экране.
 */
    sf::Vector2f getPosition() const;

/**
    * @brief Возвращает текущее направление движения бота.
 */
    int getCurrentDirection() const;

/**
    * @brief Обрабатывает столкновение бота с препятствием.
    * @param window Окно, в котором находится препятствие.
 */

    void BotBarrierBump(sf::RenderWindow & window);

/**
    * @brief Метод, не позволяющий ботам наезжать друг на друга.
 */
    friend void differentBotPosition(tankBot& firstBot, tankBot & secondBot);

/**
    * @brief Осуществляет выстрел бота.
 */
    void Shoot(){
        if(isLife) {
            if ((int) clock.getElapsedTime().asMicroseconds() % 5000 < 5 /*&& this->isFrozen == false*/) {
                float tx, ty;
                Bullet *bullet = nullptr;
                tx = this->BotSprite.getPosition().x;
                ty = this->BotSprite.getPosition().y;
                switch (this->currentDirection) {
                    case Direction::Up:
                        bullet = new EnemyBullet(static_cast<Bullet::Direction>(Direction::Up));
                        ty -= 16;
                        break;
                    case Direction::Down:
                        bullet = new EnemyBullet(static_cast<Bullet::Direction>(Direction::Down));
                        ty += 16;
                        break;
                    case Direction::Left:
                        bullet = new EnemyBullet(static_cast<Bullet::Direction>(Direction::Left));
                        tx -= 16;
                        break;
                    case Direction::Right:
                        bullet = new EnemyBullet(static_cast<Bullet::Direction>(Direction::Right));
                        tx += 16;
                        break;
                }
                bullet->sprite.setPosition(tx, ty);
                bulletList.push_back(bullet);
                clock.restart();
            }
        }
    }

/**
    * @brief Осуществляет выстрел на базу.
 */
    void baseShoot() {
        if (isLife) {
            if ((int)clock.getElapsedTime().asMicroseconds() % 5000 < 5) {
                CommandPost base;
                float distanceToBase = getDistance(BotSprite, base.getSprite());
                if (distanceToBase < 400.f) {
                    float tx, ty;
                    Bullet *bullet = nullptr;
                    tx = this->BotSprite.getPosition().x;
                    ty = this->BotSprite.getPosition().y;

                    // Вектор направления к базе
                    sf::Vector2f directionVector = base.getPosition() - this->BotSprite.getPosition();

                    // Нормализованный вектор для получения единичного вектора направления пули от бота до базы
                    sf::Vector2f normalizedDirection = directionVector / std::sqrt(directionVector.x * directionVector.x + directionVector.y * directionVector.y);

                    bullet = new EnemyBullet(normalizedDirection);
                    bullet->sprite.setPosition(tx, ty);
                    bulletList.push_back(bullet);
                    clock.restart();
                }
            }
        }
    }

/**
    * @brief Обрабатывает столкновение бота с пулями игрока.
    * @param projectiles Список пуль игрока.
 */
    void checkCollisionWithPlayer(std::deque<std::unique_ptr<Projectile>>& projectiles);

/**
    * @brief Возвращает значение флага, жив ли бот.
    * @return true - если бот жив, false - в противном случае.
 */
    bool getIsLife() const;

/**
    * @brief Возвращает расстояние между двумя объектами типа Sprite.
    * @param sprite1 Первый спрайт.
    * @param sprite2 Второй спрайт.
    * @return float Расстояние между спрайтами.
 */
    float getDistance(const sf::Sprite& sprite1, const sf::Sprite& sprite2);

    void setDeadBots() {
        deadBots++;
    }

private:
/**
    * @brief Метод, отвечающий за построение бота.
 */
    void BotBuildSprite();

    sf::Texture BotTexture;                                     /**< Текстура бота */
    sf::Sprite BotSprite;                                       /**< Спрайт бота */
    sf::Vector2f BotPosition;                                   /**< Позиция бота на карте */
    sf::Clock clock;                                            /**< Часы для отслеживания времени */
    sf::Clock clockForRestartShoot;                             /**< Часы для отслеживания времени */
    enum Direction { Up, Down, Left, Right };                   /**< Направление движения */
    Direction currentDirection;                                 /**< Текущее направление движения */
    double SpriteScale = 1;                                     /**< Масштаб танка */
    float BotMovementSpeed = BotDefaultMovementSpeed;           /**< Скорость передвижения бота */
    bool isFrozen = false;                                      /**< Переменнная, отвечающая за заморозку бота */
    bool isLife = true;                                         /**< Переменная, отвечающая за состояние жизни бота */
    int deadBots;
};


#endif //TANK_PRO_TANKBOT_H