#include <SFML/Graphics.hpp>
#include "GameMenu.hpp"
#include "../Tank/Tank.h"
#include "../Map/Map.h"
#include "../Bot/tankBot.h"
#include "../Tank/TankChoice.h"
#include "../LifeBar/LifeBar.h"
#include "../EnemyBar/EnemyBar.h"
#include "CommandPost/CommandPost.h"
#include "SetTankFunction.h"
#include "Map/MapChoice.h"
#include "TwoPlayer/TwoPlayer.h"
#include "HealthPack.h"

using namespace game;

// нужны глобально для доступа к ним методов классов
enum MenuState { MAIN_MENU, PLAY, TWOPLAY, OPTIONS, ABOUT , EXIT_MENU, NEW_GAME, PL};
enum  GameState {CONTINUE, PAUSED, GTM, INGAME};
enum TwoPlayerGameState {TWO_PLAYER_CONTINUE, TWO_PLAYER_PAUSED, TWO_PLAYER_GTM,TWO_PLAYER_INGAME};
enum OptionsState {TANK, MAP, SOUND, EXIT, OPT, ONE_PLAYER, TWO_PLAYERS};

MenuState currentMenuState = MAIN_MENU;
GameState currentGameState = INGAME;
TwoPlayerGameState currentTwoPlayerState = TWO_PLAYER_INGAME;
OptionsState currentOptionsState = OPT;

int main() {

    std::string typeOfMap = "summer";
    TwoPlayerGame tow;
    sf::RenderWindow window(sf::VideoMode(1920, 1080), "War Thunder", sf::Style::Fullscreen);

    auto m_tank = std::make_unique<KV1>();

    game::GameMenu gameMenu("gameMenu");
    game::GameMenu pauseMenu("pauseMenu");
    game::GameMenu optionsMenu("optionsMenu");
    game::GameMenu exitMenu("exitMenu");
    game::GameMenu playersMenu("players");

    sf::Music playPhonk;
    playPhonk.openFromFile("../Sounds/MainMenu.ogg");
    sf::Music welcome;
    welcome.openFromFile("../Sounds/welcome.ogg");
    sf::Music battle;
    battle.openFromFile("../Sounds/battle.ogg");
    sf::Music main;
    main.openFromFile("../Sounds/main.ogg");

    playPhonk.setVolume(50);
    welcome.setVolume(100);
    battle.setVolume(50);
    main.setVolume(40);

    welcome.play();
    main.play();

    window.setVerticalSyncEnabled(true);

    sf::RectangleShape background;
    background.setSize(sf::Vector2f(1920, 1080));
    sf::Texture texture_window;
    /*texture_window.loadFromFile("../Image/World-of-Tanks-hot-games-HD_1680x1050.jpg");*/
    texture_window.loadFromFile("../Image/main.jpg");
    background.setTexture(&texture_window);

    sf::RectangleShape background_play;
    background_play.setSize(sf::Vector2f(1920, 1080));
    sf::Texture texture_play;
    texture_play.loadFromFile("../Image/map.jpg");
    background_play.setTexture(&texture_play);

    sf::RectangleShape background_opt;
    background_opt.setSize(sf::Vector2f(1920, 1080));
    sf::Texture texture_opt;
    texture_opt.loadFromFile("../Image/options.jpg");
    background_opt.setTexture(&texture_opt);

    sf::RectangleShape background_ab;
    background_ab.setSize(sf::Vector2f(1920, 1080));
    sf::Texture texture_ab;
    texture_ab.loadFromFile("../Image/about.jpeg");
    background_ab.setTexture(&texture_ab);

    sf::Image Map_Image;
    Map_Image.loadFromFile("../Image/Merged_document.png");
    sf::Texture map;//текстура карты
    map.loadFromImage(Map_Image);//заряжаем текстуру картинкой
    sf::Sprite s_map;//создаём спрайт для карты
    s_map.setTexture(map);//заливаем текстуру спрайтом

    sf::RectangleShape background_pause;
    background_pause.setSize(sf::Vector2f(1920, 1080));
    sf::Texture texture_pause;
    texture_pause.loadFromFile("../Image/pause.jpeg");
    background_pause.setTexture(&texture_pause);
    sf::Color visibility = background_pause.getFillColor();
    visibility.a = 150;
    background_pause.setFillColor(visibility);

    sf::Texture aid;
    aid.loadFromFile("../Image/aid.png");

    LifeBar commandPostBar;

    LifeBar lifeBarPlayer;

    CommandPost commandPost;

    HealthPack healthPack;
    sf::Clock healthPackTimer;

    sf::Sprite background_die;
    sf::Texture texture_die;
    texture_die.loadFromFile("../Image/YouDie_screen.png");
    background_die.setTexture(texture_die);

    sf::Sprite background_win;
    sf::Texture texture_win;
    texture_win.loadFromFile("../Image/win.png");
    background_win.setTexture(texture_win);

    sf::RectangleShape bots;
    bots.setSize(sf::Vector2f(400, 400));
    sf::Texture texture_bots;
    texture_bots.loadFromFile("../Image/about.jpeg");
    bots.setTexture(&texture_bots);

    EnemyBar enemyBar;
    sf::Clock clockForRestartShoot;
    sf::Clock clockForRespawn;
    std::list<tankBot> botsArray(1);
    float deltaTime = 0.f;
    float spawnTimer = 0.f;


    Map mapping;
    Map map2;

    sf::Font font;
    if (!font.loadFromFile("../Image/Shrift.ttf")) {
        return EXIT_FAILURE;
    }
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(24);
    text.setFillColor(sf::Color::White);

    int deadBotsLevel1 = 0;

    while (window.isOpen())
    {
        sf::Event event{};
        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::KeyReleased:
                    if (event.key.code == sf::Keyboard::Up) {
                        gameMenu.MoveUp("gameMenu");
                    }
                    else if (event.key.code == sf::Keyboard::Down) {
                        gameMenu.MoveDown("gameMenu");
                    }
                    else if (event.key.code == sf::Keyboard::Return) {
                        int x = gameMenu.getMainMenuPressed();
                        if (x == 0) {
                            currentMenuState = PLAY;
                            main.stop();
                            battle.play();
                            playPhonk.play();

                        } else if (x == 1) {
                            currentMenuState = OPTIONS;
                            main.stop();
                        }
                        else if (x==2) {
                            currentMenuState = ABOUT;
                        }
                        else if (x == 3) {
                            currentMenuState = EXIT_MENU;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        window.draw(background);

        if (currentMenuState == MAIN_MENU) {
            gameMenu.Draw(window, "gameMenu");
        }

        if (currentMenuState == PLAY) {
            if (optionsMenu.choice == 1) {
                currentMenuState = TWOPLAY;
                optionsMenu.choice = 0;
            }
            mapping.drawMap(window, "summer");
            mapping.drawMap(window, typeOfMap);
            m_tank->drawOn(window);


            deltaTime = clockForRespawn.restart().asSeconds();
            spawnTimer += deltaTime;

            mapping.drawMap(window, typeOfMap);
            lifeBarPlayer.draw(window, 10, 10, 20, 16); //рисуем полоску здоровья
            m_tank->drawOn(window);
            commandPost.drawOn(window);
            commandPostBar.draw(window, 1014, 492, 1024, 498);

            lifeBarPlayer.update(m_tank->get_m_life());
            enemyBar.update(botsArray.size());
            commandPostBar.update(commandPost.getPostHealth());                                                      // Обновляем полоску здоровья базы в соответствии со значение здоровья базы


            sf::FloatRect sprite1Bounds = m_tank->getHull().getSprite().getGlobalBounds();
            sf::FloatRect commandPostBounds(937,553, 128, 128);                      // Делаем прямоугольник базы, чтобы знать его границы.

            if (spawnTimer > 2.f and botsArray.size()<4) {
                botsArray.emplace_back();
                spawnTimer = 0.f;
                enemyBar.update(botsArray.size());
            }

            for (auto bot = botsArray.begin(); bot != botsArray.end(); ) {
                bot->drawOn(window);
                bot->Shoot();
                bot->baseShoot();

                if (!bot->getIsLife()) {
                    bot = botsArray.erase(bot);
                    deadBotsLevel1++;
                } else {
                    ++bot;
                }
            }


            for (auto& item : bulletList) {
                item->Move();
                item->Draw(window);
            }

            for (auto bot = botsArray.begin(); bot != botsArray.end(); ++bot){
                bot->checkCollisionWithPlayer(m_tank->getProjectiles());
                if (!bot->getIsLife()){
                    botsArray.erase(bot++);
                    deadBotsLevel1++;
                }
            }

            for (auto bot = botsArray.begin(); bot != botsArray.end(); ++bot) {
                for (auto bot2 = botsArray.begin(); bot2 != botsArray.end(); ++bot2) {
                    if (bot!=bot2) differentBotPosition(*bot, *bot2);
                }
            }

            // Смотрим попадание бота в базу
            for(auto item = bulletList.begin(); item != bulletList.end() ; item++) {
                sf::FloatRect enemyBulletBounds = (*item)->getSprite().getGlobalBounds();
                if (commandPostBounds.intersects(enemyBulletBounds)) {
                    commandPost.update();
                    delete (*item);
                    bulletList.erase(item);
                    item = bulletList.begin();
                    break;
                }
            }

            for(auto item = bulletList.begin(); item != bulletList.end() ; item++) {
                sf::FloatRect sprite2Bounds = (*item)->getSprite().getGlobalBounds();
                if (sprite1Bounds.intersects(sprite2Bounds)) {
                    m_tank->m_life_damage();
                    delete (*item);
                    bulletList.erase(item);
                    item = bulletList.begin();
                    break;
                }
            }

            if (m_tank->get_m_life() == 0 || commandPost.getPostHealth() == 0) {
                botsArray.clear();
                m_tank->getHull().set_m_movementSpeed(0.f);
                game::GameMenu dieMenu("gameMenu");
                dieMenu.GameOver(window, background_die, deadBotsLevel1);
                lifeBarPlayer.draw(window, 10, 10, 20, 16); //рисуем полоску здоровья
                commandPost.drawOn(window);
                commandPostBar.draw(window, 1014, 492, 1024, 498);
            }

            if (deadBotsLevel1 == 5) {
                botsArray.clear();
                m_tank->getHull().set_m_movementSpeed(0.f);
                game::GameMenu winMenu("mainMenu");
                winMenu.GameWin(window, background_win, deadBotsLevel1);
                lifeBarPlayer.draw(window, 10, 10, 20, 16); //рисуем полоску здоровья
                commandPost.drawOn(window);
                commandPostBar.draw(window, 1014, 492, 1024, 498);
                currentMenuState = MAIN_MENU;
            }

            enemyBar.drawOn(window);

            healthPack.drawPack(window);

            sf::FloatRect tankBounds = m_tank->getHull().getSprite().getGlobalBounds();
            sf::FloatRect healthPackBounds = healthPack.getSprite().getGlobalBounds();

            if (tankBounds.intersects(healthPackBounds)) {
                m_tank->setAidXP();
                lifeBarPlayer.update(m_tank->get_m_life());
            }

            sf::Event event2{};
            while (window.pollEvent(event2)){
                if (event2.type == sf::Event::KeyReleased && event2.key.code == sf::Keyboard::Escape) {
                    currentGameState = PAUSED;
                }
                /*if (event2.type == sf::Event::KeyReleased && event2.key.code == sf::Keyboard::Q) {
                    m_tank->m_life_damage();
                }*/

            }
            if (currentGameState == PAUSED) {
                playPhonk.pause();
                window.draw(background_pause);
                pauseMenu.Draw(window, "pauseMenu");
                for(auto & bot: botsArray){
                    bot.Stop();
                }
                for (auto& item : bulletList) {
                    item->Stop();
                }
                switch (event2.type) {
                    case sf::Event::KeyReleased:
                        if (event2.key.code == sf::Keyboard::Up) {
                            pauseMenu.MoveUp("pauseMenu");
                        }
                        if (event2.key.code == sf::Keyboard::Down) {
                            pauseMenu.MoveDown("pauseMenu");
                        }
                        else if (event2.key.code == sf::Keyboard::Return) {
                            int x = pauseMenu.getPauseMenuPressed();
                            if (x == 0) {
                                currentGameState = CONTINUE;
                            }

                            else  if (x==1){
                                botsArray.clear();
                                bulletList.clear();
                                m_tank->set_m_life(100);
                                lifeBarPlayer.update(100);
                                commandPost.setPostHealth(100);
                                commandPostBar.update(100);
                                m_tank->setPosition(Vector2f(300,300));
                                m_tank->resetRotation();
                                currentGameState=CONTINUE;
                                m_tank->getHull().setPosition(sf::Vector2f (600,200));
                                lifeBarPlayer.draw(window, 10, 10, 20, 16);
                                commandPost.drawOn(window);
                                commandPostBar.draw(window, 1014, 492, 1024, 498);
                            }
                            else if (x == 2) {
                                currentGameState = GTM;
                            }
                        }
                        break;
                    default:
                        break;
                }
                if (currentGameState == CONTINUE) {
                    for(auto & bot: botsArray){
                        bot.Start();
                    }
                    for (auto& item : bulletList) {
                        item->Start();
                    }
                    playPhonk.play();
                    window.clear();
                }
                if (currentGameState == GTM) {
                    botsArray.clear();
                    bulletList.clear();

                    m_tank->set_m_life(100);
                    m_tank->setPosition(Vector2f(300,300));
                    m_tank->resetRotation();

                    commandPost.setPostHealth(100);
                    currentMenuState = MAIN_MENU;
                    main.play();
                }
            }

            if (m_tank->get_m_life() == 0 || commandPost.getPostHealth() == 0){                                          // Проверка смерти танка, т.е. начала новой игры
                game::GameMenu newGameMenu("gameMenu");
                currentMenuState = MAIN_MENU;
                botsArray.clear();
                bulletList.clear();
                m_tank->set_m_life(100);
                lifeBarPlayer.update(100);
                commandPost.setPostHealth(100);
                commandPostBar.update(100);
                m_tank->setPosition(Vector2f(300,300));
                m_tank->resetRotation();
                commandPost.setPostHealth(100);
                commandPost.drawOn(window);

            }

            /*if (deadBotsLevel1 == 5) {
                game::GameMenu GameMenu("mainMenu");
                window.clear();
                botsArray.clear();
                bulletList.clear();
                m_tank->set_m_life(100);
                lifeBarPlayer.update(100);
                commandPost.setPostHealth(100);
                commandPostBar.update(100);
                m_tank->setPosition(Vector2f(300,300));
                m_tank->resetRotation();
                commandPost.setPostHealth(100);
                commandPost.drawOn(window);
                currentMenuState = MAIN_MENU;
                map2.drawMap(window, "winter");
            }*/
        }

        if (currentMenuState==TWOPLAY)
        {
            mapping.drawMap(window, typeOfMap);
            tow.drawOn(window);
            commandPost.drawOn(window);

            sf::Event event2{};
            while (window.pollEvent(event2)){
                if (event2.type == sf::Event::KeyReleased && event2.key.code == sf::Keyboard::Escape) {
                    currentTwoPlayerState = TWO_PLAYER_PAUSED;
                }
            }

            if (tow.firstPlayer.getPlayerXP() == 0 or tow.secondPlayer.getPlayerXP() == 0) {
                game::GameMenu newGameMenu("gameMenu");
                newGameMenu.GameOver(window, background_die, 1);

                tow.firstPlayer.BulletListOfPlayer.clear();
                tow.secondPlayer.BulletListOfPlayer.clear();

                tow.restartGame();

                botsArray.clear();
                bulletList.clear();
                m_tank->set_m_life(100);
                m_tank->setPosition(Vector2f(300,300));
                m_tank->resetRotation();
                commandPost.setPostHealth(100);

                currentMenuState = MAIN_MENU;
            }

            if (currentTwoPlayerState == TWO_PLAYER_PAUSED) {
                playPhonk.pause();
                window.draw(background_pause);
                pauseMenu.Draw(window, "pauseMenu");
                tow.Stop();
                switch (event2.type) {
                    case sf::Event::KeyReleased:
                        if (event2.key.code == sf::Keyboard::Up) {
                            pauseMenu.MoveUp("pauseMenu");
                        }
                        if (event2.key.code == sf::Keyboard::Down) {
                            pauseMenu.MoveDown("pauseMenu");
                        }
                        else if (event2.key.code == sf::Keyboard::Return) {
                            int x = pauseMenu.getPauseMenuPressed();
                            if (x == 0) {
                                currentTwoPlayerState = TWO_PLAYER_CONTINUE;
                                tow.Start();
                            }
                            else  if (x==1){
                                currentTwoPlayerState=TWO_PLAYER_CONTINUE;
                                tow.Start();
                                tow.restartGame();
                            }
                            else if (x == 2) {
                                tow.restartGame();
                                currentTwoPlayerState = TWO_PLAYER_GTM;
                            }
                        }
                        break;
                    default:
                        break;
                }

                if (currentTwoPlayerState == TWO_PLAYER_CONTINUE) {
                    playPhonk.play();
                    window.clear();
                }
                if (currentTwoPlayerState == TWO_PLAYER_GTM) {
                    currentMenuState = MAIN_MENU;
                    main.play();
                }
            }
        }

        if (currentMenuState == OPTIONS) {
            window.clear();
            window.draw(background_opt);
            optionsMenu.Draw(window, "optionsMenu");
            sf::Event event3{};
            while (window.pollEvent(event3)) {
                switch (event3.type) {
                    case sf::Event::KeyReleased:

                        if (event3.key.code == sf::Keyboard::Up) {
                            optionsMenu.MoveUp("optionsMenu");
                        }
                        if (event3.key.code == sf::Keyboard::Down) {
                            optionsMenu.MoveDown("optionsMenu");
                        }
                        else if (event3.key.code == sf::Keyboard::Return) {
                            int x = optionsMenu.getOptionsMenuPressed();
                            if (x == 0) {
                                currentOptionsState = TWO_PLAYERS;
                            } else if (x == 1) {
                                currentOptionsState = TANK;
                            } else if (x == 2) {
                                currentOptionsState = MAP;
                            } else if (x == 3) {
                                currentOptionsState = SOUND;
                            } else if (x == 4) {
                                currentOptionsState = EXIT;
                                currentMenuState = MAIN_MENU;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            if (currentOptionsState == TWO_PLAYERS) {
                /*currentOptionsState = OPT;*/
               optionsMenu.choice = 1;
                /*currentMenuState = TWOPLAY;*/
            }

            if (currentOptionsState == TANK) {
                TankChoice tank;
                int res = tank.setTank(window);
                if (res == 1) {
                        m_tank->getHull().setTexture("Image/ground_shaker_asset/Red/Bodies/body_tracks.png");
                        /*m_tank->setHullTexture("Image/ground_shaker_asset/Red/Bodies/body_tracks.png");
                        m_tank->setTurretTexture("Image/ground_shaker_asset/Red/Weapons/turret_01_mk11.png");*/
                    currentMenuState = MAIN_MENU;
                    currentOptionsState = OPT;
                }
                if (res == 2) {
                    m_tank->getHull().setTexture("../Image/ground_shaker_asset/Red/Bodies/body_tracks.png");
                    m_tank->setHullTexture("../Image/ground_shaker_asset/Red/Bodies/body_tracks.png");
                    m_tank->setTurretTexture("../Image/ground_shaker_asset/Red/Weapons/turret_01_mk1.png");
                    currentMenuState = MAIN_MENU;
                    currentOptionsState = OPT;
                }
            }
            if (currentOptionsState == MAP) {
                MapChoice map;
                int res = map.setMap(window);
                if (res == 1) {
                    typeOfMap = "winter";
                    currentOptionsState = OPT;
                }
                if (res == 2) {
                    typeOfMap = "summer";
                    currentOptionsState = OPT;
                }
                if (res == 3) {
                    typeOfMap = "autumn";
                    currentOptionsState = OPT;
                }
            }
            if (currentOptionsState == OPT) {
                currentMenuState = OPTIONS;
            }
        }
        if (currentMenuState == ABOUT) {
            window.clear();
            window.draw(background_ab);
            gameMenu.ManualInAbout(window);
            sf::Event event4{};
            while(window.pollEvent(event4)) {
                if (event4.type == sf::Event::KeyReleased && event4.key.code == sf::Keyboard::Escape) {
                    currentMenuState = MAIN_MENU;
                }
            }
        }

        if (currentMenuState == EXIT_MENU) {
            window.clear();
            window.draw(background_pause);
            exitMenu.Draw(window, "exitMenu");
            sf::Event event5{};
            while (window.pollEvent(event5)) {
                if (event5.type == sf::Event::KeyReleased && event5.key.code == sf::Keyboard::Up) {
                    exitMenu.MoveUp("exitMenu");
                }
                if (event5.type == sf::Event::KeyReleased && event5.key.code == sf::Keyboard::Down) {
                    exitMenu.MoveDown("exitMenu");
                } else if (event5.type == sf::Event::KeyReleased && event5.key.code == sf::Keyboard::Return) {
                    int x = exitMenu.getExitMenuPressed();
                    if (x == 0) {
                        return 0;
                    } else if (x == 1) {
                        currentMenuState = MAIN_MENU;
                    }
                }
            }
        }
        window.display();
    }
    return 0;
}
