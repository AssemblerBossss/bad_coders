#ifndef TANK_PRO_COMMANDPOST_H
#define TANK_PRO_COMMANDPOST_H
#include <SFML/Graphics.hpp>

/**
 * @class CommandPost
 * @brief Класс, представляющий базу
 */
class CommandPost {
private:
    int PostHealth;                                         ///< Здоровье базы
    sf::Texture PostTexture;                                ///< Текстура базы
    sf::Sprite PostSprite;                                  ///< Спрайт базы
    sf::Vector2f PostPosition;                              ///< Позиция базы на экране
public:
    /**
     * @brief Конструктор класса CommandPost.
     * Устанавливает здоровье базы, загружает текстуру базы,
     * задает масштаб и начальную позицию спрайта базы.
     */
    CommandPost() {
        PostHealth = 100;
        PostTexture.loadFromFile("../Image/base.jpg");
        PostSprite.setTexture(PostTexture);
        PostSprite.setScale(0.5f, 0.5f);
        PostSprite.setPosition(937, 553);
    }

    /**
     * @brief Метод получения здоровья базы.
     * @return Здоровье базы.
     */
    int getPostHealth() const { return PostHealth; }

    /**
     * @brief Метод установки здоровья базы.
     * @param PostHealth Здоровье базы.
     */
    void setPostHealth(int PostHealth) { this->PostHealth = PostHealth; }

    /**
     * @brief Метод обновления здоровья базы.
     * Здоровье базы уменьшается на 10, если становится меньше 0,
     * здоровье устанавливается в 0.
     */
    void update() {
        PostHealth -= 10;
        if (PostHealth < 0) PostHealth = 0;
    }

    /**
     * @brief Метод отрисовки базы на экране.
     * @param window Окно, на котором идет отрисовка.
     */
    void drawOn(sf::RenderWindow& window) { window.draw(PostSprite); }

    /**
     * @brief Метод получения спрайта базы.
     * @return Спрайт базы.
     */
    sf::Sprite getSprite() {
        return  PostSprite;
    }

    /**
     * @brief Метод получения позиции базы.
     * @return Позиция базы на экране.
     */
    sf::Vector2f getPosition() const {
        return PostSprite.getPosition();
    }
};
#endif //TANK_PRO_COMMANDPOST_H
