/*
#ifndef TANK_PRO_SETTANKFUNCTION_H
#define TANK_PRO_SETTANKFUNCTION_H
#include "pch.h"
#include "Tank/TankChoice.h"
std::pair<int, bool> setTank(sf::RenderWindow& window,int Type)
{
    TankChoice blue(Type == 1? "blue": "Winter", Type == 1 ?"../Image/ground_shaker_asset/Blue/Bodies/blue.png" : "../Image/ground_shaker_asset/Terrains/winter.png");
    TankChoice red(Type == 1? "red": "Summer", Type == 1 ?"../Image/ground_shaker_asset/Red/Bodies/red.png" : "../Image/ground_shaker_asset/Terrains/leto.png");
    TankChoice camo(Type == 1? "camo" : "Tropics", Type == 1 ?"../Image/ground_shaker_asset/Camo/Bodies/camo.png": "../Image/ground_shaker_asset/Terrains/leto2.png");

    std::vector<TankChoice> tanks = {blue, red, camo};

    sf::RectangleShape tankChoice;
    tankChoice.setSize(sf::Vector2f(1920, 1080));
    sf::Texture texture_tank;
    texture_tank.loadFromFile("../Image/tankChoice.jpg");
    tankChoice.setTexture(&texture_tank);

    float spacing = 400.0f;
    float spacing2 = 437.0f;
    float startTankX = 390.0f;
    float startTextX = 450.0f;

    for (auto & tank : tanks) {
        tank._tankSprite.setPosition(startTankX, 300.0f);
        tank._tankSprite.setOrigin(tank._tankSprite.getGlobalBounds().width / 2, tank._tankSprite.getGlobalBounds().height / 2);
        tank._tankName.setPosition(startTankX, 450.0f);
        tank._tankName.setOrigin(tank._tankName.getGlobalBounds().width / 2, tank._tankName.getGlobalBounds().height / 2);
        startTankX += tank._tankSprite.getGlobalBounds().width + spacing;
        startTextX += tank._tankName.getGlobalBounds().width + spacing2;
    }

    int static  selectedTank = 0;
    int totalTank = 0;

    bool returnPressed = false;

    sf::Event event{};
    while (event.key.code != sf::Keyboard::S) {
        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::KeyPressed:
                    switch (event.key.code) {
                        case sf::Keyboard::Left:
                            selectedTank = (selectedTank - 1 + 3) % 3;
                            break;
                        case sf::Keyboard::Right:
                            selectedTank = (selectedTank + 1) % 3;
                            break;
                        case sf::Keyboard::S:
                            if (selectedTank >= 0 && selectedTank < 3) {
                                totalTank = selectedTank + 1;
                                returnPressed = true;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

        }

        window.draw(tankChoice);

        for (size_t i = 0; i < tanks.size(); ++i) {
            window.draw(tanks[i]._tankSprite);
            window.draw(tanks[i]._tankName);

            if (i == selectedTank) {
                tanks[i]._tankName.setFillColor(sf::Color::Yellow);
                tanks[i]._tankSprite.setScale(1.5f, 1.5f);
            }
            else {
                tanks[i]._tankName.setFillColor(sf::Color::White);
                tanks[i]._tankSprite.setScale(1.0f, 1.0f);
            }
        }
        window.display();

        if (returnPressed) {
            return std::make_pair(totalTank, true);
        }
    }

    return std::make_pair(0, false);
}
#endif //TANK_PRO_SETTANKFUNCTION_H
*/
