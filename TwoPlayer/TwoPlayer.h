#include "../pch.h"
#include "../Bullet/Bullet.h"
#include "../LifeBar/LifeBar.h"

#ifndef TANK_PRO_TWOPLAYER_H
#define TANK_PRO_TWOPLAYER_H

class TwoPlayer;

class FirstPlayer;

class SecondPlayer;

class TwoPlayerGame;

class TwoPlayer {
public:
    TwoPlayer();

    ~TwoPlayer() = default;

    static constexpr float kMainWeaponSecondsToReload = 12;     // Скорость перезарядки
    constexpr static float BotDefaultMovementSpeed = 1.0f;      // Скорость передвижения игроков
    float sumTimeMainWeaponToReload = 0.f;                      // Общее время для перезарядки орудия

    void BotSetTexture();                                       // Метод, отвечающий за устаноку текстуры игрока

    void drawOn(sf::RenderWindow &window);                      // Метод, отвечающий за отрисовку игрока на карте

    void Stop();                                                // Метод, отвечающий за остановку игрока во время паузы

    void Start();                                               // Метод, отвечающий за начало игры после остановки

    void BotBarrierBump(sf::RenderWindow &window);              // Метод, отвечающий за рельеф местности

    void Shoot();                                               // Метод, отвечающий за произведение выстрела

    void takingDamage();                                        // Получение урона от попадания снаряда

    void BotBuildSprite();                                      // Метод, отвечающий за построение спрайта

    void setPosition(sf::Vector2f newPlayerPosition);           // Метод, отвечающий за перенос танка по карте

    sf::Vector2f getPosition() const;                           // Метод, возвращающий позицию игрока на карте

    int getCurrentDirection() const;                            // Метод, возвращающий текущее направление движения

    bool getIsLife() const;                                     // Метод, возвращающий состояние жизни игрока

    int getPlayerXP() const;                                    // Метод, возвращающий XP игрока

    std::list<Bullet *> returnBulletList() const;

    sf::Texture BotTexture;                                     // Текстура игрока
    sf::Sprite BotSprite;                                       // Спрайт игрока
    sf::Vector2f BotPosition;                                   // Позиция игрока на карте
    sf::Clock clock;                                            // Часы для отслеживания времени

    enum Direction {
        Up, Down, Left, Right
    };                                                          // Направление движения
    Direction currentDirection;                                 // Текущее направление движения
    double SpriteScale = 1;                                     // Масштаб танка
    float BotMovementSpeed = BotDefaultMovementSpeed;           // Скорость передвижения игрока
    bool isFrozen = false;                                      // Переменнная, отвечающая за заморозку игрока
    bool isLife = true;                                         // Переменная, отвечающая за состояние жизни игрока
    int XP = 100;                                               // Переменная, отвечающая за количества XP игрока
    int Angle = 0;                                              // Переменная, отвечающая за угол поворота спрайта
    std::list<Bullet *> BulletListOfPlayer;                     // Массив снарядов каждого игрока

};

class SecondPlayer : public TwoPlayer {
public:

    SecondPlayer();

    void updatePosition();                                      // Метод, отвечающий за управление вторым танком
    void intersectionWithPlayer(FirstPlayer &object);           // Получение урона от вражеских снарядов

    LifeBar CurrentHealth;                                      // Бар, отображащий XP игрока
    sf::Vector2f startPosition {1500,200};                // Начальная позиция игрока
};

class FirstPlayer : public TwoPlayer {
public:

    FirstPlayer();

    void updatePosition();                                      // Метод, отвечающий за управление вторым танком
    void intersectionWithPlayer(SecondPlayer &object);          // Получение урона от вражеских снарядов

    LifeBar CurrentHealth;                                      // Бар, отображащий XP игрока
    sf::Vector2f startPosition {200,200};                 // Начальная позиция игрока
};

class TwoPlayerGame {
public:

    void drawOn(sf::RenderWindow &window);                      // Метод, отвечающий за отрисовку игроков

    void Stop();                                                // Метод, отвечающий за остановку игроков при нажатии Escape

    void Start();                                               // Метод, отвечающий за продолжение игры

    void restartGame();                                         // Метод, отвечающий за рестарт игры

    FirstPlayer firstPlayer;
    SecondPlayer secondPlayer;
};

void TwoPlayer::drawOn(sf::RenderWindow &window) {
    for (auto &item: BulletListOfPlayer) {
        item->Move();
    }
    if (isLife) {
        for (auto &item: BulletListOfPlayer) {
            item->Move();
            item->Draw(window);
        }
        window.draw(BotSprite);
    }
}

TwoPlayer::TwoPlayer() {
    currentDirection = Up;
    BotSetTexture();
}

void TwoPlayer::BotSetTexture() {                                                  // Загрузка текстуры танка-бота
    if (!BotTexture.loadFromFile("../Image/track.png")) {
        std::cerr << "Failed to load tank bot texture!" << std::endl;
    }
    BotSprite.setTexture(BotTexture);
    BotBuildSprite();
}

void TwoPlayer::BotBuildSprite() {
    sf::Vector2f startPostion(300.f, 150.f);
    BotSprite.setScale(SpriteScale, SpriteScale);
    BotSprite.setPosition(startPostion);
    BotSprite.setOrigin(sf::Vector2f(BotTexture.getSize() / 2u));
    BotPosition = startPostion;
}

void TwoPlayer::BotBarrierBump(sf::RenderWindow &window) {}

int TwoPlayer::getCurrentDirection() const {
    return currentDirection;
}

void TwoPlayer::Stop() {
    isFrozen = true;
}

void TwoPlayer::Start() {
    isFrozen = false;
}

bool TwoPlayer::getIsLife() const {
    return isLife;
}

sf::Vector2f TwoPlayer::getPosition() const {
    return BotSprite.getPosition();
}

std::list<Bullet *> TwoPlayer::returnBulletList() const {
    return BulletListOfPlayer;
}

void TwoPlayer::Shoot() {
    if (isLife) {
        sumTimeMainWeaponToReload += (float) clock.getElapsedTime().asSeconds();
        if (sumTimeMainWeaponToReload > kMainWeaponSecondsToReload && !this->isFrozen) {
            float tx, ty;
            Bullet *bullet = nullptr;
            tx = this->BotSprite.getPosition().x;
            ty = this->BotSprite.getPosition().y;
            switch (this->currentDirection) {
                case Direction::Up:
                    bullet = new EnemyBullet(static_cast<Bullet::Direction>(Direction::Up));
                    ty -= 16;
                    break;
                case Direction::Down:
                    bullet = new EnemyBullet(static_cast<Bullet::Direction>(Direction::Down));
                    ty += 16;
                    break;
                case Direction::Left:
                    bullet = new EnemyBullet(static_cast<Bullet::Direction>(Direction::Left));
                    tx -= 16;
                    break;
                case Direction::Right:
                    bullet = new EnemyBullet(static_cast<Bullet::Direction>(Direction::Right));
                    tx += 16;
                    break;
            }
            bullet->sprite.setPosition(tx, ty);
            BulletListOfPlayer.push_back(bullet);
            clock.restart();
            sumTimeMainWeaponToReload = 0;
        }
    }
}

void TwoPlayer::takingDamage() {
    this->XP -= 20;
    if (this->XP <= 0) isLife = false;
}

int TwoPlayer::getPlayerXP() const {
    return this->XP;
}

void TwoPlayer::setPosition(sf::Vector2f newPlayerPosition) {
    this->BotPosition = newPlayerPosition;
}

FirstPlayer::FirstPlayer(): TwoPlayer() {
    this->setPosition(this->startPosition);
}

SecondPlayer::SecondPlayer(): TwoPlayer() {
    this->setPosition(this->startPosition);
}

void FirstPlayer::updatePosition() {
    if (!isFrozen) {
        BotMovementSpeed = BotDefaultMovementSpeed;
        this->CurrentHealth.update(this->getPlayerXP());
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W)) {
            currentDirection = TwoPlayer::Direction::Up;
            BotPosition.y -= BotMovementSpeed;
            Angle = 270;
        } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A)) {
            currentDirection = TwoPlayer::Direction::Left;
            BotPosition.x -= BotMovementSpeed;
            Angle = 180;
        } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S)) {
            currentDirection = TwoPlayer::Direction::Down;
            BotPosition.y += BotMovementSpeed;
            Angle = 90;
        } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)) {
            currentDirection = TwoPlayer::Direction::Right;
            BotPosition.x += BotMovementSpeed;
            Angle = 0;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift)) {
            this->Shoot();
        }

        BotSprite.setPosition(BotPosition);  // Обновляем позицию спрайта
    } else {
        BotMovementSpeed = 0;
    }
    BotSprite.setRotation(Angle);
}

void SecondPlayer::updatePosition() {
    if (!isFrozen) {
        BotMovementSpeed = BotDefaultMovementSpeed;
        this->CurrentHealth.update(this->getPlayerXP());
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Up)) {
            currentDirection = TwoPlayer::Direction::Up;
            BotPosition.y -= BotMovementSpeed;
            Angle = 270;
        } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left)) {
            currentDirection = TwoPlayer::Direction::Left;
            BotPosition.x -= BotMovementSpeed;
            Angle = 180;
        } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Down)) {
            currentDirection = TwoPlayer::Direction::Down;
            BotPosition.y += BotMovementSpeed;
            Angle = 90;
        } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right)) {
            currentDirection = TwoPlayer::Direction::Right;
            BotPosition.x += BotMovementSpeed;
            Angle = 0;
        }
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::RShift)) {
            this->Shoot();
        }
        BotSprite.setRotation(static_cast<float>(Angle));
        BotSprite.setPosition(BotPosition);  // Обновляем позицию спрайта
    } else {
        BotMovementSpeed = 0;
    }
}

void FirstPlayer::intersectionWithPlayer(SecondPlayer &object) {
    sf::FloatRect SecondPlayerBounds = this->BotSprite.getGlobalBounds();
    for (auto item = object.BulletListOfPlayer.begin(); item != object.BulletListOfPlayer.end(); item++) {
        sf::FloatRect sprite2Bounds = (*item)->getSprite().getGlobalBounds();
        if (SecondPlayerBounds.intersects(sprite2Bounds)) {
            FirstPlayer::takingDamage();
            delete (*item);
            bulletList.erase(item++);
            item = object.BulletListOfPlayer.begin();
            break;
        }
    }
}

void SecondPlayer::intersectionWithPlayer(FirstPlayer &object) {
    sf::FloatRect SecondPlayerBounds = this->BotSprite.getGlobalBounds();
    for (auto item = object.BulletListOfPlayer.begin(); item != object.BulletListOfPlayer.end(); item++) {
        sf::FloatRect sprite2Bounds = (*item)->getSprite().getGlobalBounds();
        if (SecondPlayerBounds.intersects(sprite2Bounds)) {
            SecondPlayer::takingDamage();
            delete (*item);
            bulletList.erase(item++);
            item = object.BulletListOfPlayer.begin();
            break;
        }
    }
}

void TwoPlayerGame::drawOn(RenderWindow &window) {
    firstPlayer.CurrentHealth.draw(window, 10, 10, 20, 16);
    secondPlayer.CurrentHealth.draw(window, 1770, 10, 1780, 16);

    firstPlayer.intersectionWithPlayer(secondPlayer);
    secondPlayer.intersectionWithPlayer(firstPlayer);

    firstPlayer.updatePosition();
    firstPlayer.drawOn(window);

    secondPlayer.updatePosition();
    secondPlayer.drawOn(window);
}

void TwoPlayerGame::Start() {
    firstPlayer.Start();
    secondPlayer.Start();
}

void TwoPlayerGame::Stop() {
    firstPlayer.Stop();
    secondPlayer.Stop();
}

void TwoPlayerGame::restartGame() {
    this->firstPlayer.setPosition(firstPlayer.startPosition);
    this->secondPlayer.setPosition(secondPlayer.startPosition);

    this->firstPlayer.isLife = true;
    this->secondPlayer.isLife = true;

    this->firstPlayer.XP = 100;
    this->secondPlayer.XP = 100;

    this->firstPlayer.CurrentHealth.update(firstPlayer.XP);
    this->secondPlayer.CurrentHealth.update(secondPlayer.XP);
}

#endif
