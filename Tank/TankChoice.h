//
// Created by max on 20.11.2023.
//

#ifndef TANK_PRO_TANKCHOICE_H
#define TANK_PRO_TANKCHOICE_H
#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>
using namespace std;
struct TankChoice
{
    sf::Sprite _tankSprite;
    sf::Text _tankName;
    sf::Texture _tankTexture;
    sf::Font _font;

    TankChoice() {

    }

    TankChoice(const std::string& name, const std::string& path)
    {
        _font.loadFromFile("../Image/Shrift.ttf");

        _tankName.setFont(_font);
        _tankName.setFillColor(sf::Color::White);
        _tankName.setString(name);
        _tankName.setCharacterSize(60);
        _tankName.setOutlineThickness(3);
        _tankName.setOutlineColor(sf::Color::Black);

        _tankTexture.loadFromFile(path);

        _tankSprite.setTexture(_tankTexture);
    }

    int setTank(sf::RenderWindow& window)
    {
        TankChoice blue("blue", "../Image/ground_shaker_asset/Blue/Bodies/blue.png");
        TankChoice red("red", "../Image/ground_shaker_asset/Red/Bodies/red.png");
        TankChoice camo("camo", "../Image/ground_shaker_asset/Camo/Bodies/camo.png");

        std::vector<TankChoice> tanks = {blue, red, camo};

        sf::RectangleShape tankChoice;
        tankChoice.setSize(sf::Vector2f(1920, 1080));
        sf::Texture texture_tank;
        texture_tank.loadFromFile("../Image/tankChoice.jpg");
        tankChoice.setTexture(&texture_tank);

        float spacing = 400.0f;
        float spacing2 = 437.0f;
        float startTankX = 390.0f;
        float startTextX = 450.0f;

        for (auto & tank : tanks) {
            tank._tankSprite.setPosition(startTankX, 300.0f);
            tank._tankSprite.setOrigin(tank._tankSprite.getGlobalBounds().width / 2, tank._tankSprite.getGlobalBounds().height / 2);
            tank._tankName.setPosition(startTankX, 450.0f);
            tank._tankName.setOrigin(tank._tankName.getGlobalBounds().width / 2, tank._tankName.getGlobalBounds().height / 2);
            startTankX += tank._tankSprite.getGlobalBounds().width + spacing;
            startTextX += tank._tankName.getGlobalBounds().width + spacing2;
        }

        int static  selectedTank = 0;
        int static totalTank = 0;

        bool returnPressed = false;

        sf::Event event{};
        while (event.key.code != sf::Keyboard::S) {
            while (window.pollEvent(event)) {
                switch (event.type) {
                    case sf::Event::Closed:
                        window.close();
                        break;
                    case sf::Event::KeyPressed:
                        switch (event.key.code) {
                            case sf::Keyboard::Left:
                                selectedTank = (selectedTank - 1 + 3) % 3;
                                break;
                            case sf::Keyboard::Right:
                                selectedTank = (selectedTank + 1) % 3;
                                break;
                            case sf::Keyboard::S:
                                if (selectedTank >= 0 && selectedTank < 3) {
                                    totalTank = selectedTank + 1;
                                    returnPressed = true;
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }

            }

            window.draw(tankChoice);

            for (size_t i = 0; i < tanks.size(); ++i) {
                window.draw(tanks[i]._tankSprite);
                window.draw(tanks[i]._tankName);

                if (i == selectedTank) {
                    tanks[i]._tankName.setFillColor(sf::Color::Yellow);
                    tanks[i]._tankSprite.setScale(1.5f, 1.5f);
                }
                else {
                    tanks[i]._tankName.setFillColor(sf::Color::White);
                    tanks[i]._tankSprite.setScale(1.0f, 1.0f);
                }
            }
            window.display();

            if (returnPressed) {
                return totalTank;
            }
        }

        return 0;
    }
};

#endif //TANK_PRO_TANKCHOICE_H
