#include "Turret.hpp"

/**
    @brief Конструктор класса Turret
    @param hull Ссылка на объект класса Hull
*/
Turret::Turret(const Hull &hull) : Changedhull(hull) {
    m_texture.loadFromFile("../Image/ground_shaker_asset/Camo/Weapons/turret_01_mk11.png");
    buildSprite();
}

/**
    * @brief Метод, который обновляет поворот турели на основе позиции указанного окна
    * @param window Окно, на котором происходит обновление поворота
 */
void Turret::updateRotation(const sf::RenderWindow &window) {
    m_sprite.setPosition(Changedhull.getSprite().getPosition());

    // Calculate the angle between the turret and the mouse cursor
    const auto mousePosition = sf::Vector2f(sf::Mouse::getPosition(window));
    const sf::Vector2f delta(mousePosition - getPosition());
    const float angle = std::atan2(delta.y, delta.x) * 180.0f / static_cast<float>(3.14);

    rotate(angle);
}

/**
    @brief Метод, который отрисовывает спрайт турели на указанном окне
    @param window Окно, на котором отрисовывается спрайт
 */
void Turret::drawOn(sf::RenderWindow &window) {
    //Cначала вызывается метод `updateRotation()` для обновления поворота турели. Затем спрайт турели рисуется на окне.
    updateRotation(window);
    window.draw(m_sprite);
}

/**
    * @brief Метод, который возвращает позицию снаряда, вылетающего из турели.
    * Он вычисляет позицию снаряда на основе направления турели и размеров спрайта турели.
 */
sf::Vector2f Turret::getMuzzlePosition() const {
    PolarVector vector = m_directionVector;
    vector.radius *= static_cast<float>(m_texture.getSize().x) * m_sprite.getScale().x / 2.0f;
    return getPosition() + vector.getDecartVector();
}

/**
    * @brief Метод, который возвращает позицию турели.
 */
sf::Vector2f Turret::getPosition() const {
    return m_sprite.getPosition();
}

/**
    * @brief Метод, который возвращает ссылку на спрайт турел
 */
sf::Sprite &Turret::getSprite() {
    return m_sprite;
}

/**
    * @brief Устанавливает текстуру для спрайта турели
    * @param path Путь до устанавливаемой текстуры
 */
void Turret::setTexture(const std::string &path) {
    //if (!m_texture.loadFromFile(path.data())) {
    //std::runtime_error("Missing texture for turret: ");
    //  return;
    //}
    m_texture.loadFromFile(path);
    m_sprite.setTexture(m_texture);
}

/**
    * @brief Метод, который поворачивает турель на указанный угол в радианах
    * @param angle Угол поворота в радианах
 */
void Turret::rotate(const float &angle) {
    if (angle == m_directionVector.angle)
        return;
    m_sprite.setRotation(angle);
    m_directionVector.angle = angle;
}

/**
    @brief Метод, который строит спрайт турели
*/
void Turret::buildSprite() {
    m_sprite.setScale(Hull::SpriteScale, Hull::SpriteScale);
    m_sprite.setOrigin(sf::Vector2f(m_texture.getSize() / 2u));
    m_sprite.setPosition(Changedhull.getSprite().getPosition());
}

/**
    * @brief Метод, который возвращает вектор направления турели в координатах полярной системы.
 */
sf::Vector2f Turret::getDirectionVector() const {
    return m_directionVector.getDecartVector();
}
