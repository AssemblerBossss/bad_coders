/**
    @file Turret.hpp
    @brief Заголовочный файл класса Turret
 */

#pragma once

#include "../Entity/entity.h"
#include "MovementVectors.hpp"
#include "Hull.hpp"
#include "vector"

#ifndef TANK_PRO_TURRET_HPP
#define TANK_PRO_TURRET_HPP

/**
    @class Turret
    @brief Класс, представляющий башню танка
*/
class Turret : public Entity {
public:
    const Hull &Changedhull;                                     /**<  Константная ссылка на объект класса `Hull`, который представляет корпус, к которому привязана турель */
/**
    @brief Конструктор класса Turret
    @param hull Ссылка на объект класса Hull
*/
    explicit Turret(const Hull &hull);

/**
    @brief Деструктор класса Turret
 */
    ~Turret() override = default;

/**
    @brief Метод, который отрисовывает спрайт турели на указанном окне
    @param window Окно, на котором отрисовывается спрайт
 */
    void drawOn(sf::RenderWindow &window) override;

/**
    @brief Метод, который строит спрайт турели
*/
    void buildSprite();

/**
    * @brief Метод, который поворачивает турель на указанный угол в радианах
    * @param angle Угол поворота в радианах
 */
    void rotate(const float &angle);

/**
    * @brief Метод, который обновляет поворот турели на основе позиции указанного окна
    * @param window Окно, на котором происходит обновление поворота
 */
    void updateRotation(const sf::RenderWindow &window);

/**
    * @brief Устанавливает текстуру для спрайта турели
    * @param path Путь до устанавливаемой текстуры
 */
    void setTexture(const std::string &path);

/**
    * @brief Метод, который возвращает позицию снаряда, вылетающего из турели.
 */
    sf::Vector2f getMuzzlePosition() const;

/**
    * @brief Метод, который возвращает позицию турели.
 */
    sf::Vector2f getPosition() const;

/**
    * @brief Метод, который возвращает ссылку на спрайт турел
 */
    sf::Sprite &getSprite();

/**
    * @brief Метод, который возвращает вектор направления турели в координатах полярной системы.
 */
    sf::Vector2f getDirectionVector() const;


    PolarVector m_directionVector = {1.0f,0.0f};   /**<  переменная типа `PolarVector`, представляющая направление, в котором смотрит турель в координатах полярной системы. */
    sf::Texture m_texture;                                       /**<  Приватная переменная типа `sf::Texture`, хранящая текстуру турели. */
    sf::Sprite m_sprite;                                         /**<  Приватная переменная типа `sf::Sprite`, представляющая спрайт турели. */

    std::vector<sf::Texture> massive_texture;


};

#endif //TANK_PRO_TURRET_HPP
