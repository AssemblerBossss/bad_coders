/**

    @file Hull.hpp
    @brief Заголовочный файл класса Hull
 */

#ifndef TANK_PRO_HULL_HPP
#define TANK_PRO_HULL_HPP
#pragma once

#include "../Entity/entity.h"
#include "MovementVectors.hpp"
#include "../Map/Map.h"

/**
    * @class Hull
    * @brief Класс, представляющий корпус танка
*/

class Hull : public Entity {
public:
/**
    * @brief Значение скорости перемещения по умолчанию
 */
    constexpr static float DefaultMovementSpeed = 1.0f;
/**
    * @brief Значение скорости поворота по умолчанию
*/
    constexpr static float DefaultRotationSpeed = 0.5f;
/**
    * @brief Масштаб спрайта корпуса по умолчанию
 */
    constexpr static float SpriteScale = 1.0f;

/**
    * @brief Конструктор по умолчанию
*/
    Hull();

/**
    * @brief Отрисовывает спрайт корпуса на указанном окне
    * @param window Окно, на которое нужно отрисовать спрайт
*/
    void drawOn(sf::RenderWindow &window) override;

/**
    * @brief Возвращает спрайт корпуса
    * @return Спрайт корпуса
*/
    sf::Sprite getSprite() const;

/**
    * @brief Устанавливает текстуру корпуса
    * @param path Путь к файлу с текстурой
*/
    void setTexture(/*const std::string_view &path*/const std::string &path);

    /**
        * @brief  Возвращает позицию корпуса
    */
    sf::Vector2f getPosition(){
        return m_sprite.getPosition();
    }

    /**
        * @brief  Устанавливает позицию корпуса
        * @param currentPosition Новая позиция корпуса
    */
    void setPosition(sf::Vector2f CurrentPosition){
        m_sprite.setPosition(CurrentPosition);
    }

    /**
        * @brief Устанавливает скорость перемещения
        * @param speed Новая скорость перемещения
    */
    void set_m_movementSpeed(float k){
        m_movementSpeed = k;
    }

    /**
        * @brief  Столконовение с препятствиями
        * @param window Окно, нак отором отрисовывается обьект
    */
    void barrierBump(sf::RenderWindow &window) ;

    /**
        * @brief  Разворот в исходное положение
    */
    void resetRotation();


private:
/**
    * @brief Обновляет позицию корпуса на основе его текущей скорости и направления
*/
    void updatePosition();

/**
 * @brief Создает спрайт корпуса
 */
    void buildSprite();

    float m_movementSpeed = DefaultMovementSpeed;                  ///<  Текущая скорость перемещения
    float m_rotationSpeed = DefaultRotationSpeed;                  ///<  Текущая скорость поворота корпуса

    sf::Texture m_texture;                                         ///< Текстура корпуса

    sf::Sprite m_sprite;                                           ///< Спрайт корпуса

    PolarVector m_facingDirection = {1.0f, 0.0f};    ///<  Переменная типа `PolarVector`, представляющая направление,
                                                                   ///<  в котором смотрит корпус в координатах полярной системы.
};

#endif //TANK_PRO_HULL_HPP



