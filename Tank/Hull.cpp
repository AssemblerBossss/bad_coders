
#include "Hull.hpp"

/**
    * @brief Конструктор по умолчанию
*/
Hull::Hull() {
    m_texture.loadFromFile("../Image/ground_shaker_asset/Camo/Bodies/body_tracks.png");
}

/**
    * @brief Возвращает спрайт корпуса
    * @return Спрайт корпуса
*/
sf::Sprite Hull::getSprite() const {
    return m_sprite;
}

/**
    * @brief Отрисовывает спрайт корпуса на указанном окне
    * @param window Окно, на которое нужно отрисовать спрайт
*/
void Hull::drawOn(sf::RenderWindow &window) {
    window.draw(m_sprite);
    updatePosition();
    barrierBump(window);
}

/**
    * @brief Обновляет позицию корпуса на основе его текущей скорости и направления
*/
void Hull::updatePosition() {
    const sf::Vector2f changeVector = m_facingDirection.getDecartVector() * m_movementSpeed;

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W)) {
        m_sprite.move(changeVector.x, changeVector.y);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A)) {
        m_sprite.rotate(-m_rotationSpeed);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S)) {
        m_sprite.move(-changeVector.x, -changeVector.y);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)) {
        m_sprite.rotate(m_rotationSpeed);
    }

    m_facingDirection.angle = m_sprite.getRotation();
}

/**
    * @brief Устанавливает текстуру корпуса
    * @param path Путь к файлу с текстурой
*/
void Hull::setTexture(/*const std::string_view &path*/const std::string &path) {
    if (!m_texture.loadFromFile(path)) {
        return;
    }
    m_texture.loadFromFile(/*"../Image/ground_shaker_asset/Camo/Bodies/body_tracks.png"*/path);
    m_sprite.setTexture(m_texture);
    buildSprite();
}

/**
 * @brief Создает спрайт корпуса
 */
void Hull::buildSprite() {

    const sf::Vector2f startPostion(600.f, 200.f);
    m_sprite.setScale(SpriteScale, SpriteScale);
    m_sprite.setPosition(startPostion);
    m_sprite.setOrigin(sf::Vector2f(m_texture.getSize() /2u));
}

/**
    * @brief  Разворот в исходное положение
*/
void Hull::resetRotation() {
    m_sprite.setRotation(0.0f);
    m_facingDirection.angle = 0;
}

/**
    * @brief  Столконовение с препятствиями
    * @param window Окно, нак отором отрисовывается обьект
*/
void Hull::barrierBump(sf::RenderWindow &window) {

    // 1. Танк не заезжает за поле
    if (this->getPosition().x > 1860.f) {
        this->setPosition(sf::Vector2f(1859, this->getPosition().y));
    } else if (this->getPosition().x < 60) {
        this->setPosition(sf::Vector2f(61, this->getPosition().y));
    }
    if (this->getPosition().y < 60) {
        this->setPosition(sf::Vector2f(this->getPosition().x, 62));
    } else if (this->getPosition().y > 1020) {
        this->setPosition(sf::Vector2f(this->getPosition().x, 1018));
    }


    // 2. Танк не заезжает в первый прямоугольник
    if (this->getPosition().x > 364 and this->getPosition().x < 492 and this->getPosition().y > 364 and this->getPosition().y < 748) {
        this->setPosition(sf::Vector2f(363, this->getPosition().y)); // левая сторона
    }

    if (this->getPosition().x < 520 and this->getPosition().x > 364 and this->getPosition().y > 364 and this->getPosition().y < 748) {
        this->setPosition(sf::Vector2f(521, this->getPosition().y)); // правая сторона
    }

    if (this->getPosition().y > 362 and this->getPosition().y < 750 and this->getPosition().x > 364 and this->getPosition().x < 520) {
        this->setPosition(sf::Vector2f(this->getPosition().x, 361)); // верхняя сторона
    }

    if (this->getPosition().y < 780 and this->getPosition().y > 363 and this->getPosition().x > 364 and this->getPosition().x < 519) {
        this->setPosition(sf::Vector2f(this->getPosition().x, 781)); // нижняя сторона
    }


    // 3. Танк не заезжает в башню
    if (this->getPosition().x > 880 and this->getPosition().x < 1024 and this->getPosition().y > 512 and this->getPosition().y < 651) {
        this->setPosition(sf::Vector2f(879, this->getPosition().y)); // левая сторона
    }

    if (this->getPosition().x < 1036 and this->getPosition().x > 880 and this->getPosition().y > 512 and this->getPosition().y < 651) {
        this->setPosition(sf::Vector2f(1037, this->getPosition().y)); // правая сторона
    }

    if (this->getPosition().y > 500 and this->getPosition().y < 642 and this->getPosition().x > 880 and this->getPosition().x < 1025) {
        this->setPosition(sf::Vector2f(this->getPosition().x, 501)); // верхняя сторона
    }

    if (this->getPosition().y < 656 and this->getPosition().y > 510 and this->getPosition().x > 880 and this->getPosition().x < 1024) {
        this->setPosition(sf::Vector2f(this->getPosition().x, 653)); // нижняя сторона
    }


    // 4. Танк не заезжает в первый прямоугольник
    if (this->getPosition().x > 1400 and this->getPosition().x < 1546 and this->getPosition().y > 364 and this->getPosition().y < 778 ) {
        this->setPosition(sf::Vector2f(1399, this->getPosition().y)); // левая сторона
    }

    if (this->getPosition().x < 1556 and this->getPosition().x > 1408 and this->getPosition().y > 364 and this->getPosition().y < 748) {
        this->setPosition(sf::Vector2f(1558, this->getPosition().y)); // правая сторона
    }

    if (this->getPosition().y > 362 and this->getPosition().y < 750 and this->getPosition().x > 1408 and this->getPosition().x < 1546) {
        this->setPosition(sf::Vector2f(this->getPosition().x, 361)); // верхняя сторона
    }

    if (this->getPosition().y < 780 and this->getPosition().y > 363 and this->getPosition().x > 1408 and this->getPosition().x < 1546) {
        this->setPosition(sf::Vector2f(this->getPosition().x, 781)); // нижняя сторона
    }
}
