/**
    @file Turret.hpp
    @brief Заголовочный файл класса Turret
 */
#pragma once

#include <cmath>
#include "SFML/Graphics.hpp"

#ifndef TANK_PRO_MOVEMENTVECTORS_HPP
#define TANK_PRO_MOVEMENTVECTORS_HPP


/**
    @class PolaVector
    @brief Класс, представляющий перевод в полярную систему координат
*/
struct PolarVector {
    [[nodiscard]] float getX() const {
        float radians = angle * (3.14159f / 180.0f);
        return radius * std::cos(radians);
    }

    [[nodiscard]] float getY() const {
        float radians = angle * (3.14159f / 180.0f);
        return radius * std::sin(radians);
    }

    [[nodiscard]] sf::Vector2f getDecartVector() const {
        return {getX(), getY()};
    }

    float radius = 1.0f;
    float angle = 0.0f;
};

#endif //TANK_PRO_MOVEMENTVECTORS_HPP
