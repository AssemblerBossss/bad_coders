#include "../Projectiles/Projectile.h"
#include "../Projectiles/Config.h"
#include "Hull.hpp"
#include "Turret.hpp"
#include "../Entity/entity.h"
#include "../pch.h"


#ifndef TANK_PRO_TANK_H
#define TANK_PRO_TANK_H

class Tank : public Entity {
public:
    static constexpr float kMainWeaponSecondsToReload = 1.0f;/**<  Приватная переменная, отвечающая за время перезарядки орудия */

    ~Tank() override = default;

/**
 * @brief Устанавливает позицию танка
 * @param position Позиция танка
 */
    void setPosition(sf::Vector2f position){
        this->m_hull->setPosition(position);
        this->m_turret->getSprite().setPosition(position);
    }

/**
 * @brief Сбрасывает вращение танка
*/
    void resetRotation() {
        this->m_hull->resetRotation();
    }

/**
 * @brief Отрисовка спрайта игрока
*/
    void drawOn(sf::RenderWindow &window) override {
        m_hull->drawOn(window);
        m_turret->drawOn(window);

        for (const auto &projectile: m_projectiles) {
            projectile->drawOn(window);
        }

        handleShoot(window);
    }

/**
 * @brief Возвращает корпус танка
 * @return Корпус танка
 */
    [[nodiscard]] Hull getHull() const {
        return *m_hull;
    }

/**
 * @brief Наносит урон хп танка
*/
    void m_life_damage(){
        m_life = m_life - 20;
    }

    void setAidXP() {
        if (m_life < 100) m_life = m_life + 20;
    }

/**
 * @brief Устанавливает хп танка
 * @param life Хп танка
 */
    void set_m_life(int life){
        if(m_life < 100) m_life = life;
    }

/**
 * @brief Возвращает хп танка
 * @return Хп танка
 */
    [[maybe_unused]] [[nodiscard]] int get_m_life() const {
        return m_life;
    }

/**
 * @brief Возвращает проектиты танка
 * @return Проектиты танка
*/
    [[nodiscard]] std::deque<std::unique_ptr<Projectile>> &getProjectiles() {
        return m_projectiles;
    }

    explicit Tank() {
        m_hull = std::make_unique<Hull>();
        m_turret = std::make_unique<Turret>(*m_hull);
    }

/**
 * @brief Устанавливает текстуру корпуса
 *
 * @param path Путь до текстуры
 */
    void setHullTexture(/*const std::string_view &path*/const std::string &path) {
        m_hull->setTexture(path);
    }

/**
 * @brief Устанавливает текстуру башни
 *
 * @param path Путь до текстуры
 */
    void setTurretTexture(const std::string &path) {
        m_turret->setTexture(path);
    }

/**
 * @brief Возвращает башню танка
 *
 * @return Башня танка
 */
    [[nodiscard]] Turret getTurret() const {
        return *m_turret;
    }

/**
    * @brief Обрабатывает выстрелы танка
    * @param window Окно приложения
 */
    void handleShoot(sf::RenderWindow &window) {
        if (sf::Mouse::isButtonPressed(Configuration::Controls::MAIN_WEAPON)) {
            if (m_shellClock.getElapsedTime().asSeconds() > kMainWeaponSecondsToReload) {
                getProjectiles().push_front(std::make_unique<Shell>(getTurret()));
                m_shellClock.restart();
                int i = 1;
                while (i < 31) {
                    std::string path = "../Image/ground_shaker_asset/Camo/Gun/" + std::to_string(i) + ".png";
                    m_turret->setTexture(path);

                    m_turret->drawOn(window);
                    i++;
                }
                m_turret->setTexture("../Image/ground_shaker_asset/Camo/Weapons/turret_01_mk11.png");
                i = 0;
            }

        }
        while ((!getProjectiles().empty() && !getProjectiles().front()->isWithinScreenBounds())) {
            getProjectiles().front().reset();
            getProjectiles().pop_front();
        }
    }

private:
    int m_life = 100;                                                   /**<  Приватная переменная, отвечающая за жизнь игрока */
    std::unique_ptr<Hull> m_hull;                                       /**<  Уникальный указатель на корпус */
    std::unique_ptr<Turret> m_turret;                                   /**<  Уникальный указатель на башню. */
    std::deque<std::unique_ptr<Projectile>> m_projectiles;              /**<  Очередь снарядов */
    sf::Clock m_shellClock;                                             /**<  Приватная переменная, отвечающая за перезарядку */
};

/**
    @brief Класс для создания танка KV1
*/
class KV1 final : public Tank {
public:
    explicit KV1() {

        setHullTexture(/*"../Image/hull.png"*/"../Image/ground_shaker_asset/Camo/Bodies/body_tracks.png");
        setTurretTexture("../Image/ground_shaker_asset/Camo/Weapons/turret_01_mk11.png");
    }

    ~KV1() override = default;
};

/**
    @brief Класс для создания красного танка
*/
class Red final : public Tank {
public:
    explicit Red() {

        setHullTexture("../Image/ground_shaker_asset/Red/Bodies/body_tracks.png");
        setTurretTexture("../Image/ground_shaker_asset/Red/Weapons/turret_01_mk11.png");
    }

    ~Red() override = default;
};

/**
    @brief Класс для создания синего танка
*/
class Blue final : public Tank {
public:
    explicit Blue() {

        setHullTexture("../Image/ground_shaker_asset/Blue/Bodies/body_tracks.png");
        setTurretTexture("../Image/ground_shaker_asset/Blue/Weapons/turret_01_mk11.png");
    }

    ~Blue() override = default;
};


#endif //TANK_PRO_TANK_H
