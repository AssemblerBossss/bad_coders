#include "GameMenu.hpp"

/**
 * @brief Установка начальных параметров шрифта и текста.
 *
 * @param text - Ссылка на текст.
 * @param str - Строка для текста.
 * @param ypos - Положение текста по оси Y.
 * @param xpos - Положение текста по оси X.
 */
void game::GameMenu::setInitFont(sf::Text &text, const std::string &str, float ypos, float xpos) {
    text.setFont(font);
    text.setFillColor(sf::Color::White);
    text.setString(str);
    text.setCharacterSize(60);
    text.setPosition(xpos, ypos);
    text.setOutlineThickness(3);
    text.setOutlineColor(sf::Color::Black);
}

/**
 * @brief Конструктор класса GameMenu.
 *
 * @param typeOfMenu - Тип меню (MAIN, PAUSE, OPTIONS или EXIT).
 */
game::GameMenu::GameMenu(const std::string &typeOfMenu) {
    if (!font.loadFromFile("../Image/Shrift.ttf")) {
        std::cout << "No font is here";
    }

    if (typeOfMenu == "gameMenu") {
        for (int i = 0, ypos1 = 150, xpos1 = 540; i < max_menu; i++, ypos1 += 100)
            setInitFont(mainMenu[i], str[i], ypos1, xpos1);
        mainMenuSelected = -1;
    }

    if (typeOfMenu == "pauseMenu") {
        for (int i = 0, ypos1 = 420, xpos1 = 750; i < max_menu_pause; i++, ypos1 += 100)
            setInitFont(pauseMenu[i], str2[i], ypos1, xpos1);
        pauseMenuSelected = -1;
    }

    if (typeOfMenu == "optionsMenu") {
        for (int i = 0, ypos1 = 150, xpos1 = 540; i < max_menu_options; i++, ypos1 += 100)
            setInitFont(optionsMenu[i], str3[i], ypos1, xpos1);
        optionsMenuSelected = -1;
    }

    if (typeOfMenu == "exitMenu") {
        setInitFont(exitText, exitStr, 320, 560);
        for (int i = 0, ypos1 = 420, xpos1 = 830; i < max_menu_exit; i++, ypos1 += 100)
            setInitFont(exitMenu[i], str4[i], ypos1, xpos1);
        exitMenuSelected = -1;
    }
}

/**
 * @brief Перемещение курсора в меню вверх.
 *
 * @param typeOfMenu - Тип меню (MAIN, PAUSE, OPTIONS или EXIT).
 */
void game::GameMenu::MoveUp(const std::string &typeOfMenu) {
    if (typeOfMenu == "gameMenu") {
        if (mainMenuSelected == 0) {
            mainMenu[0].setFillColor(sf::Color::White);
            mainMenuSelected = max_menu - 1;
            mainMenu[mainMenuSelected].setFillColor(sf::Color::Yellow);
        } else {
            mainMenu[mainMenuSelected--].setFillColor(sf::Color::White);
            mainMenu[mainMenuSelected].setFillColor(sf::Color::Yellow);
        }
    } else if (typeOfMenu == "pauseMenu") {
        if (pauseMenuSelected == 0) {
            pauseMenu[0].setFillColor(sf::Color::White);
            pauseMenuSelected = max_menu_pause - 1;
            pauseMenu[pauseMenuSelected].setFillColor(sf::Color::Yellow);
        } else {
            pauseMenu[pauseMenuSelected--].setFillColor(sf::Color::White);
            pauseMenu[pauseMenuSelected].setFillColor(sf::Color::Yellow);
            if (pauseMenuSelected < 0) pauseMenuSelected = 3;
            pauseMenu[pauseMenuSelected].setFillColor(sf::Color::Yellow);
        }
    } else if (typeOfMenu == "optionsMenu") {
        if (optionsMenuSelected == 0) {
            optionsMenu[0].setFillColor(sf::Color::White);
            optionsMenuSelected = max_menu_options - 1;
            optionsMenu[optionsMenuSelected].setFillColor(sf::Color::Yellow);
        } else {
            optionsMenu[optionsMenuSelected--].setFillColor(sf::Color::White);
            optionsMenu[optionsMenuSelected].setFillColor(sf::Color::Yellow);
        }
    } else if (typeOfMenu == "exitMenu") {
        if (exitMenuSelected == 0) {
            exitMenu[0].setFillColor(sf::Color::White);
            exitMenuSelected = max_menu_exit - 1;
            exitMenu[exitMenuSelected].setFillColor(sf::Color::Yellow);
        } else {
            exitMenu[exitMenuSelected--].setFillColor(sf::Color::White);
            exitMenu[exitMenuSelected].setFillColor(sf::Color::Yellow);
        }
    }
}

/**
 * @brief Перемещение курсора в меню вниз.
 *
 * @param typeOfMenu - Тип меню (MAIN, PAUSE, OPTIONS или EXIT).
 */
void game::GameMenu::MoveDown(const std::string &typeOfMenu) {
    if (typeOfMenu == "gameMenu") {
        if (mainMenuSelected + 1 <= max_menu) {
            mainMenu[mainMenuSelected].setFillColor(sf::Color::White);
            mainMenuSelected++;
            if (mainMenuSelected == 4) mainMenuSelected = 0;
            mainMenu[mainMenuSelected].setFillColor(sf::Color::Yellow);
        }
    }

    if (typeOfMenu == "pauseMenu") {
        if (pauseMenuSelected + 1 <= max_menu_pause) {
            pauseMenu[pauseMenuSelected].setFillColor(sf::Color::White);
            pauseMenuSelected++;
            if (pauseMenuSelected == 3) pauseMenuSelected = 0;
            pauseMenu[pauseMenuSelected].setFillColor(sf::Color::Yellow);
        }
    }

    if (typeOfMenu == "optionsMenu") {
        if (optionsMenuSelected + 1 <= max_menu_options) {
            optionsMenu[optionsMenuSelected].setFillColor(sf::Color::White);
            optionsMenuSelected++;
            if (optionsMenuSelected == 5) optionsMenuSelected = 0;
            optionsMenu[optionsMenuSelected].setFillColor(sf::Color::Yellow);
        }
    }

    if (typeOfMenu == "exitMenu") {
        if (exitMenuSelected + 1 <= max_menu_exit) {
            exitMenu[exitMenuSelected].setFillColor(sf::Color::White);
            exitMenuSelected++;
            if (exitMenuSelected == 2) exitMenuSelected = 0;
            exitMenu[exitMenuSelected].setFillColor(sf::Color::Yellow);
        }
    }
}

game::GameMenu::~GameMenu()= default;

/**
 * @brief Отрисовка меню.
 *
 * @param window - Ссылка на окно, в котором будет отрисовываться меню.
 * @param typeOfMenu - Тип меню (MAIN, PAUSE, OPTIONS или EXIT).
 */
void game::GameMenu::Draw(sf::RenderWindow &window, const std::string& typeOfMenu) {
    if (typeOfMenu == "gameMenu") {
        for (const auto &i: mainMenu) window.draw(i);
    }

    if (typeOfMenu == "pauseMenu") {
        for (const auto &i: pauseMenu) window.draw(i);
    }

    if (typeOfMenu == "optionsMenu") {
        for (const auto &i: optionsMenu) window.draw(i);
    }

    if (typeOfMenu == "exitMenu") {
        window.draw(exitText);
        for (const auto &i: exitMenu) window.draw(i);
    }
}

/**
 * @brief Окончание игры.
 *
 * @param window - Ссылка на окно, в котором будет отрисовываться.
 */
void game::GameMenu::GameOver(RenderWindow &window, sf::Sprite & sprite, int bots) {
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(50);
    text.setFillColor(sf::Color::Red);
    text.setPosition(window.getSize().x / 2.0f - 150.f, window.getSize().y / 2.0f + 150.f);
    text.setString("Dead Bots:" + std::to_string(bots));
    while (ScaryButton < 600) {
        float elapsed = clock.getElapsedTime().asSeconds();
        sprite.setColor(sf::Color(255, 255, 255, static_cast<sf::Uint8>(255 * elapsed / 12.0f)));
        ScaryButton += 0.05;
        window.clear();
        window.draw(sprite);
        window.draw(text);
        window.display();
        ScaryButton++;

    }
}

void game::GameMenu::GameWin(RenderWindow &window, sf::Sprite & sprite, int bots) {
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(50);
    sf::Color pink = sf::Color(255, 182, 193);
    text.setFillColor(pink);
    text.setPosition(window.getSize().x / 2.0f - 150.f, window.getSize().y / 2.0f + 150.f);
    text.setString("Dead Bots:" + std::to_string(bots));
    sprite.setPosition((window.getSize().x - sprite.getLocalBounds().width) / 2 + 30.f,
                       (window.getSize().y - sprite.getLocalBounds().height) / 2);
    while (ScaryButton < 600) {
        float elapsed = clock.getElapsedTime().asSeconds();
        sprite.setColor(sf::Color(255, 255, 255, static_cast<sf::Uint8>(255 * elapsed / 12.0f)));
        ScaryButton += 0.05;
        window.clear();
        window.draw(sprite);
        window.draw(text);
        window.display();
        ScaryButton++;
    }
}
/*void game::GameMenu::GameOver(RenderWindow &window, sf::Sprite & sprite, int bots) {
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(24);
    text.setFillColor(sf::Color::Yellow);
    text.setPosition(10.f, window.getSize().y - text.getGlobalBounds().height - 10.f);
    text.setString("Dead Bots: " + std::to_string(bots));

    // Устанавливаем начальный цвет спрайта с полностью непрозрачным альфа-каналом
    sprite.setColor(sf::Color(255, 255, 255, 255));

    while (ScaryButton < 600) {
        float elapsed = clock.getElapsedTime().asSeconds();

        // Изменяем только альфа-канал цвета спрайта
        sprite.setColor(sf::Color(255, 255, 255, static_cast<sf::Uint8>(255 * elapsed / 12.0f)));

        ScaryButton += 0.05;
        window.clear();
        window.draw(sprite);
        window.draw(text);
        window.display();
    }
}*/

/**
 * @brief Показать справку.
 *
 * @param window - Ссылка на окно, в котором будет отображаться справка.
 */
void game::GameMenu::ManualInAbout( sf::RenderWindow & window) {
    std::string textInAboutOne[5]{"For 1 player :","Up", "Right","Down", "Left" };
    std::string textInAboutTwo[12]  {"For first player :","Up", "Right","Down", "Left","Shoot", "For second player :" , "Up", "Right","Down", "Left", "Shoot" };
    for(int i=0, y=120; i<4;i++, y+=80){
        sf::Sprite ButtonSprite;
        sf::Texture ButtonTexture;
        ButtonTexture.loadFromFile("../Image/buttons/"+ std::to_string(i)+".png");
        ButtonSprite.setTexture(ButtonTexture);
        ButtonSprite.setScale(0.13f,0.13f);
        ButtonSprite.setPosition(  440,static_cast<float>(y) );
        window.draw(ButtonSprite);
    }

    for(int i=0, y=120; i<5;i++, y+=80){
        sf::Sprite ButtonSprite;
        sf::Texture ButtonTexture;
        ButtonTexture.loadFromFile("../Image/buttons/"+ std::to_string(i)+".png");
        ButtonSprite.setTexture(ButtonTexture);
        ButtonSprite.setScale(0.13f,0.13f);
        ButtonSprite.setPosition(  1650,static_cast<float>(y) );
        window.draw(ButtonSprite);
    }
    for(int i=5, y=600; i<10;i++, y+=80){
        sf::Sprite ButtonSprite;
        sf::Texture ButtonTexture;
        ButtonTexture.loadFromFile("../Image/buttons/"+ std::to_string(i)+".png");
        ButtonSprite.setTexture(ButtonTexture);
        ButtonSprite.setScale(0.13f,0.13f);
        ButtonSprite.setPosition(  1650,static_cast<float>(y) );
        window.draw(ButtonSprite);
    }
    for (int i = 0, ypos1 = 50; i <5; i++, ypos1 += 80) {
        setInitFont(aboutTextForOne[i], textInAboutOne[i], ypos1, 90);
        aboutTextForOne[i].setCharacterSize(45);
        aboutTextForOne[i].setFillColor(sf::Color::Yellow);
    }

    for (int i = 0, ypos1 = 50; i <12; i++, ypos1 += 80) {
        setInitFont(aboutTextForTwo[i], textInAboutTwo[i], ypos1, 1160);
        aboutTextForTwo[i].setCharacterSize(45);
        aboutTextForTwo[i].setFillColor(sf::Color::Yellow);
    }

    for (const auto &i: aboutTextForTwo) window.draw(i);
    for (const auto &i: aboutTextForOne) window.draw(i);

}











