//
// Created by mihail on 17.10.2023.
//

#ifndef UNTITLED_GAMEMENU_HPP
#define UNTITLED_GAMEMENU_HPP
#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>
#include <list>
#include <string>
#include <array>

using namespace sf;
namespace game {

/**
 * @class GameMenu
 *
 * @brief Класс, представляющий игровое меню.
 */
    class GameMenu
    {

        static const int max_menu = 4;                              ///< Переменная отвечает за максимальное количество пунктов меню MAIN
        static const int max_menu_pause = 3;                        ///< Переменная отвечает за максимальное количество пунктов меню PAUSE
        static const int max_menu_options = 5;                      ///< Переменная отвечает за максимальное количество пунктов меню OPTIONS
        static const int max_menu_exit = 2;

        int mainMenuSelected;                                       ///< Переменная отвечает за выбранный пукнт меню в меню MAIN
        int pauseMenuSelected;                                      ///< Переменная отвечает за выбранный пукнт меню в меню PAUSE
        int optionsMenuSelected;                                    ///< Переменная отвечает за выбранный пукнт меню в меню OPTIONS
        int exitMenuSelected;                                       ///< Переменная отвечает за выбранный пукнт меню в меню EXIT
        double ScaryButton = 0;                                     ///< Переменная отвечает за прозрачность изображения поражения игрока
        sf::Font font;
        sf::Text mainMenu[max_menu];
        sf::Text pauseMenu[max_menu_pause];
        sf::Text optionsMenu[max_menu_options];
        sf::Text exitMenu[max_menu_exit];
        sf::Text exitText;
        sf::Text aboutTextForTwo[12];
        sf::Text aboutTextForOne[5];
        /**
         * @brief Установка начальных параметров шрифта и текста.
         *
         * @param text - Ссылка на текст.
         * @param str - Строка для текста.
         * @param ypos - Положение текста по оси Y.
         * @param xpos - Положение текста по оси X.
         */
        void setInitFont(sf::Text & text, const std::string& str, float ypos, float xpos);

        std::string str[5]{ "Play","Options","About","Exit" };

        std::string str2[3]{ "Continue", "Restart","Go to main menu" };

        std::string  str3[5]{ "Two players", "Tank", "Map", "Sound", "Exit"};

        std::string str4[2]{"Yes", "No"};

        std::string exitStr="Are you sure want to exit?";



    public:
        int choice;

        /**
         * @brief Конструктор класса GameMenu.
         * @param typeOfMenu - Тип меню (MAIN, PAUSE, OPTIONS или EXIT).
        */
        explicit GameMenu(const std::string& typeOfMenu);

        /**
         * @brief Отрисовка меню.
         *
         * @param window - Ссылка на окно, в котором будет отрисовываться меню.
         * @param typeOfMenu - Тип меню (MAIN, PAUSE, OPTIONS или EXIT).
        */
        void Draw(sf::RenderWindow& window, const std::string& typeOfMenu);

        /**
         * @brief Перемещение курсора в меню вверх.
         *
         * @param typeOfMenu - Тип меню (MAIN, PAUSE, OPTIONS или EXIT).
        */
        void MoveUp(const std::string& typeOfMenu);

        /**
         * @brief Перемещение курсора в меню вниз.
         *
         * @param typeOfMenu - Тип меню (MAIN, PAUSE, OPTIONS или EXIT).
        */
        void MoveDown(const std::string& typeOfMenu);

        /**
         * @brief Показать справку.
         *
         * @param window - Ссылка на окно, в котором будет отображаться справка.
         */
        void ManualInAbout(sf::RenderWindow & window);

        /**
         * @brief Окончание игры.
         *
         * @param window - Ссылка на окно, в котором будет отрисовываться.
         */
        void GameOver(sf::RenderWindow & window, sf::Sprite & sprite, int bots);

        void GameWin(RenderWindow &window, sf::Sprite & sprite, int bots);
        /**
         * @brief Нажатая клавиша на Главном меню.

         */
        int getMainMenuPressed() const  { return mainMenuSelected;}

        /**
         * @brief Нажатая клавиша на меню паузы.

         */
        int getPauseMenuPressed() const { return pauseMenuSelected; }

        /**
         * @brief Нажатая клавиша на меню настроек.

         */
        int getOptionsMenuPressed() const { return optionsMenuSelected; }

        /**
         * @brief Нажатая клавиша на меню окончания игры.

         */
        int getExitMenuPressed() const { return exitMenuSelected; }

        ~GameMenu();

        sf::Clock clock;

    };

}
#endif //UNTITLED_GAMEMENU_HPP
